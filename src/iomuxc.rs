use core::marker::PhantomData;

use crate::dev::{self, iomuxc};
use crate::generic::*;

register_block!(IOMUXC, iomuxc);

// TODO: SION on mux
// TODO: API for pad control

// TODO: per-pin mux/pad/state traits to make it impossible to instantiate an invalid combination
// _AltState<U1>, _MuxControlField<U1>, _PadControlField<U1>
// or: _PadControlField<TMuxControl>, _AltState<TMuxControl>

pub trait _AltState {}
pub trait _MuxControlField {
    fn _get(regblock: &iomuxc::RegisterBlock) -> &Self;
}
pub trait _PadControlField {
    fn _get(regblock: &iomuxc::RegisterBlock) -> &Self;
}

pub struct Pin<TMuxControl, TPadControl, TFunction>
where
    TMuxControl: _MuxControlField,
    TPadControl: _PadControlField,
    TFunction: _Function,
{
    _mux_control: PhantomData<TMuxControl>,
    _pad_control: PhantomData<TPadControl>,
    _function: PhantomData<TFunction>,
}

impl<TMuxControl, TPadControl, TFunction> Pin<TMuxControl, TPadControl, TFunction>
where
    TMuxControl: _MuxControlField,
    TPadControl: _PadControlField,
    TFunction: _Function,
{
    pub unsafe fn fiat() -> Self {
        Self {
            _mux_control: PhantomData,
            _pad_control: PhantomData,
            _function: PhantomData,
        }
    }

    fn regblock() -> &'static iomuxc::RegisterBlock {
        unsafe { dev::IOMUXC::regblock() }
    }

    fn mux() -> &'static TMuxControl {
        TMuxControl::_get(Self::regblock())
    }

    fn pad_() -> &'static TPadControl {
        TPadControl::_get(Self::regblock())
    }

    pub fn pad(&self) -> &'static TPadControl {
        Self::pad_()
    }
}

// MACRO FODDER

macro_rules! iomuxc_pin {
    ($pin: ident, ($mux_ctl: ident, $mux_ctl_lc: ident), ($pad_ctl: ident, $pad_ctl_lc: ident),
     $( $into: ident : $fn: ty => ($alt: ident, $doc: expr), )*
    ) => {
        impl _MuxControlField for iomuxc::$mux_ctl {
            fn _get(regblock: &iomuxc::RegisterBlock) -> &Self {
                &regblock.$mux_ctl_lc
            }
        }

        impl _PadControlField for iomuxc::$pad_ctl {
            fn _get(regblock: &iomuxc::RegisterBlock) -> &Self {
                &regblock.$pad_ctl_lc
            }
        }

        pub type $pin<TFunction> = Pin<iomuxc::$mux_ctl, iomuxc::$pad_ctl, TFunction>;

        impl<TFunction> $pin<TFunction>
        where
            TFunction: _Function,
        {
            $(
                #[doc=$doc]
                pub fn $into(self) -> $pin<$fn> {
                    Self::mux().modify(|_r, w| w.mux_mode().$alt());
                    unsafe { $pin::<$fn>::fiat() }
                }
            )*

            pub fn sion(&mut self, enabled: bool) {
                Self::mux().modify(|_r, w| w.sion().bit(enabled));
            }
        }

    };
}

iomuxc_pin!(
    Pin0,
    (SW_MUX_CTL_PAD_GPIO_AD_B0_03, sw_mux_ctl_pad_gpio_ad_b0_03),
    (SW_PAD_CTL_PAD_GPIO_AD_B0_03, sw_pad_ctl_pad_gpio_ad_b0_03),
    // SION
    // ALT0: FLEXCAN2_RX
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_INOUT17"),
    into_lpuart_rx: crate::lpuart::LPUART6 => (alt2, "ALT2: LPUART6_RX"),
    // ALT3: USB_OTG1_OC
    // ALT4: FLEXPWM1_PWMX01
    into_gpio: crate::gpio::Pin0<()> => (alt5, "ALT5: GPIO1_IO03"), // reset state
    // ALT6: REF_CLK_24M
    // ALT7: LPSPI3_PCS0
);

iomuxc_pin!(
    Pin1,
    (SW_MUX_CTL_PAD_GPIO_AD_B0_02, sw_mux_ctl_pad_gpio_ad_b0_02),
    (SW_PAD_CTL_PAD_GPIO_AD_B0_02, sw_pad_ctl_pad_gpio_ad_b0_02),
    // SION
    // ALT0: FLEXCAN2_TX
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_INOUT16"),
    into_lpuart_tx: crate::lpuart::LPUART6 => (alt2, "ALT2: LPUART6_TX"),
    // ALT3: USB_OTG1_PWR
    // ALT4: FLEXPWM1_PWMX00
    into_gpio: crate::gpio::Pin1<()> => (alt5, "ALT5: GPIO1_IO02"), // reset state
    into_lpi2c: crate::lpi2c::LPI2C1 => (alt6, "ALT6: LPI2C1_HREQ"),
    // ALT7: LPSPI3_SDI
);

iomuxc_pin!(
    Pin2,
    (SW_MUX_CTL_PAD_GPIO_EMC_04, sw_mux_ctl_pad_gpio_emc_04),
    (SW_PAD_CTL_PAD_GPIO_EMC_04, sw_pad_ctl_pad_gpio_emc_04),
    // SION
    // ALT0: SEMC_DATA04
    // ALT1: FLEXPWM4_PWMA02
    // ALT2: SAI2_TX_DATA
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT06"),
    into_flexio: dev::FLEXIO1 => (alt4, "ALT4: FLEXIO1_FLEXIO04"),
    into_gpio: crate::gpio::Pin2<()> => (alt5, "ALT5: GPIO4_IO04"), // reset state
);

iomuxc_pin!(
    Pin3,
    (SW_MUX_CTL_PAD_GPIO_EMC_05, sw_mux_ctl_pad_gpio_emc_05),
    (SW_PAD_CTL_PAD_GPIO_EMC_05, sw_pad_ctl_pad_gpio_emc_05),
    // SION
    // ALT0: SEMC_DATA05
    // ALT1: FLEXPWM4_PWMB02
    // ALT2: SAI2_TX_SYNC
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT07"),
    into_flexio: dev::FLEXIO1 => (alt4, "ALT4: FLEXIO1_FLEXIO05"),
    into_gpio: crate::gpio::Pin3<()> => (alt5, "ALT5: GPIO4_IO05"), // reset state
);

iomuxc_pin!(
    Pin4,
    (SW_MUX_CTL_PAD_GPIO_EMC_06, sw_mux_ctl_pad_gpio_emc_06),
    (SW_PAD_CTL_PAD_GPIO_EMC_06, sw_pad_ctl_pad_gpio_emc_06),
    // SION
    // ALT0: SEMC_DATA06
    // ALT1: FLEXPWM2_PWMA00
    // ALT2: SAI2_TX_BCLK
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT08"),
    into_flexio: dev::FLEXIO1 => (alt4, "ALT4: FLEXIO1_FLEXIO06"),
    into_gpio: crate::gpio::Pin4<()> => (alt5, "ALT5: GPIO4_IO06"), // reset state
);

iomuxc_pin!(
    Pin5,
    (SW_MUX_CTL_PAD_GPIO_EMC_08, sw_mux_ctl_pad_gpio_emc_08),
    (SW_PAD_CTL_PAD_GPIO_EMC_08, sw_pad_ctl_pad_gpio_emc_08),
    // SION
    // ALT0: SEMC_DM00
    // ALT1: FLEXPWM2_PWMA01
    // ALT2: SAI2_RX_DATA
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT17"),
    into_flexio: dev::FLEXIO1 => (alt4, "ALT4: FLEXIO1_FLEXIO08"),
    into_gpio: crate::gpio::Pin5<()> => (alt5, "ALT5: GPIO4_IO08"), // reset state
);

iomuxc_pin!(
    Pin6,
    (SW_MUX_CTL_PAD_GPIO_B0_10, sw_mux_ctl_pad_gpio_b0_10),
    (SW_PAD_CTL_PAD_GPIO_B0_10, sw_pad_ctl_pad_gpio_b0_10),
    // SION
    // ALT0: LCD_DATA06
    into_timer: dev::TMR4 => (alt1, "ALT1: QTIMER4_TIMER1"),
    // ALT2: FLEXPWM2_PWMA02
    // ALT3: SAI1_TX_DATA03
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO10"),
    into_gpio: crate::gpio::Pin6<()> => (alt5, "ALT5: GPIO2_IO10"), // reset state
    // ALT6: SRC_BOOT_CFG06
    // ALT8: ENET2_CRS
);

iomuxc_pin!(
    Pin7,
    (SW_MUX_CTL_PAD_GPIO_B1_01, sw_mux_ctl_pad_gpio_b1_01),
    (SW_PAD_CTL_PAD_GPIO_B1_01, sw_pad_ctl_pad_gpio_b1_01),
    // SION
    // ALT0: LCD_DATA13
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_INOUT15"),
    into_lpuart_rx: crate::lpuart::LPUART4 => (alt2, "ALT2: LPUART4_RX"),
    // ALT3: SAI1_TX_DATA00
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO17"),
    into_gpio: crate::gpio::Pin7<()> => (alt5, "ALT5: GPIO2_IO17"), // reset state
    // ALT6: FLEXPWM1_PWMB03
    // ALT8: ENET2_RDATA00
    // FIXME: into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO17"),
);

iomuxc_pin!(
    Pin8,
    (SW_MUX_CTL_PAD_GPIO_B1_00, sw_mux_ctl_pad_gpio_b1_00),
    (SW_PAD_CTL_PAD_GPIO_B1_00, sw_pad_ctl_pad_gpio_b1_00),
    // SION
    // ALT0: LCD_DATA12
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_INOUT14"),
    into_lpuart_tx: crate::lpuart::LPUART4 => (alt2, "ALT2: LPUART4_TX"),
    // ALT3: SAI1_RX_DATA00
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO16"),
    into_gpio: crate::gpio::Pin8<()> => (alt5, "ALT5: GPIO2_IO16"), // reset state
    // ALT6: FLEXPWM1_PWMA03
    // ALT8: ENET2_RX_ER
    // FIXME: into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO16"),
);

iomuxc_pin!(
    Pin9,
    (SW_MUX_CTL_PAD_GPIO_B0_11, sw_mux_ctl_pad_gpio_b0_11),
    (SW_PAD_CTL_PAD_GPIO_B0_11, sw_pad_ctl_pad_gpio_b0_11),
    // SION
    // ALT0: LCD_DATA07
    into_timer: dev::TMR4 => (alt1, "ALT1: QTIMER4_TIMER2"),
    // ALT2: FLEXPWM2_PWMB02
    // ALT3: SAI1_TX_DATA02
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO11"),
    into_gpio: crate::gpio::Pin9<()> => (alt5, "ALT5: GPIO2_IO11"), // reset state
    // ALT6: SRC_BOOT_CFG07
    // ALT8: ENET2_COL
);

iomuxc_pin!(
    Pin10,
    (SW_MUX_CTL_PAD_GPIO_B0_00, sw_mux_ctl_pad_gpio_b0_00),
    (SW_PAD_CTL_PAD_GPIO_B0_00, sw_pad_ctl_pad_gpio_b0_00),
    // SION
    // ALT0: LCD_CLK
    into_timer: dev::TMR1 => (alt1, "ALT1: QTIMER1_TIMER0"),
    // ALT2: MQS_RIGHT
    // ALT3: LPSPI4_PCS0
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO00"),
    into_gpio: crate::gpio::Pin10<()> => (alt5, "ALT5: GPIO2_IO00"), // reset state
    // ALT6: SEMC_CSX01
    // ALT8: ENET2_MDC
);

iomuxc_pin!(
    Pin11,
    (SW_MUX_CTL_PAD_GPIO_B0_02, sw_mux_ctl_pad_gpio_b0_02),
    (SW_PAD_CTL_PAD_GPIO_B0_02, sw_pad_ctl_pad_gpio_b0_02),
    // SION
    // ALT0: LCD_HSYNC
    into_timer: dev::TMR1 => (alt1, "ALT1: QTIMER1_TIMER2"),
    // ALT2: FLEXCAN1_TX
    // ALT3: LPSPI4_SDO
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO02"),
    into_gpio: crate::gpio::Pin11<()> => (alt5, "ALT5: GPIO2_IO02"), // reset state
    // ALT6: SEMC_CSX03
    // ALT8: ENET2_1588_EVENT0_OUT
);

iomuxc_pin!(
    Pin12,
    (SW_MUX_CTL_PAD_GPIO_B0_01, sw_mux_ctl_pad_gpio_b0_01),
    (SW_PAD_CTL_PAD_GPIO_B0_01, sw_pad_ctl_pad_gpio_b0_01),
    // SION
    // ALT0: LCD_ENABLE
    into_timer: dev::TMR1 => (alt1, "ALT1: QTIMER1_TIMER1"),
    // ALT2: MQS_LEFT
    // ALT3: LPSPI4_SDI
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO01"),
    into_gpio: crate::gpio::Pin12<()> => (alt5, "ALT5: GPIO2_IO01"), // reset state
    // ALT6: SEMC_CSX02
    // ALT8: ENET2_MDIO
);

iomuxc_pin!(
    Pin13,
    (SW_MUX_CTL_PAD_GPIO_B0_03, sw_mux_ctl_pad_gpio_b0_03),
    (SW_PAD_CTL_PAD_GPIO_B0_03, sw_pad_ctl_pad_gpio_b0_03),
    // SION
    // ALT0: LCD_VSYNC
    into_timer: dev::TMR2 => (alt1, "ALT1: QTIMER2_TIMER0"),
    // ALT2: FLEXCAN1_RX
    // ALT3: LPSPI4_SCK
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO03"),
    into_gpio: crate::gpio::Pin13<()> => (alt5, "ALT5: GPIO2_IO03"), // reset state
    // ALT6: WDOG2_RESET_B_DEB
    // ALT8: ENET2_1588_EVENT0_IN
);

iomuxc_pin!(
    Pin14,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_02, sw_mux_ctl_pad_gpio_ad_b1_02),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_02, sw_pad_ctl_pad_gpio_ad_b1_02),
    // SION
    // ALT0: USB_OTG1_ID
    into_timer: dev::TMR3 => (alt1, "ALT1: QTIMER3_TIMER2"),
    into_lpuart_tx: crate::lpuart::LPUART2 => (alt2, "ALT2: LPUART2_TX"),
    // ALT3: SPDIF_OUT
    // ALT4: ENET_1588_EVENT2_OUT
    into_gpio: crate::gpio::Pin14<()> => (alt5, "ALT5: GPIO1_IO18"), // reset state
    // ALT6: USDHC1_CD_B
    // ALT7: KPP_ROW06
    // ALT8: GPT2_CLK
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO02"),
);

iomuxc_pin!(
    Pin15,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_03, sw_mux_ctl_pad_gpio_ad_b1_03),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_03, sw_pad_ctl_pad_gpio_ad_b1_03),
    // SION
    // ALT0: USB_OTG1_OC
    into_timer: dev::TMR3 => (alt1, "ALT1: QTIMER3_TIMER3"),
    into_lpuart_rx: crate::lpuart::LPUART2 => (alt2, "ALT2: LPUART2_RX"),
    // ALT3: SPDIF_IN
    // ALT4: ENET_1588_EVENT2_IN
    into_gpio: crate::gpio::Pin15<()> => (alt5, "ALT5: GPIO1_IO19"), // reset state
    // ALT6: USDHC2_CD_B
    // ALT7: KPP_COL06
    // ALT8: GPT2_CAPTURE1
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO03"),
);

iomuxc_pin!(
    Pin16,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_07, sw_mux_ctl_pad_gpio_ad_b1_07),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_07, sw_pad_ctl_pad_gpio_ad_b1_07),
    // SION
    // ALT0: FLEXSPIB_DATA00
    into_lpi2c: crate::lpi2c::LPI2C3 => (alt1, "ALT1: LPI2C3_SCL"),
    into_lpuart_rx: crate::lpuart::LPUART3 => (alt2, "ALT2: LPUART3_RX"),
    // ALT3: SPDIF_EXT_CLK
    // ALT4: CSI_HSYNC
    into_gpio: crate::gpio::Pin16<()> => (alt5, "ALT5: GPIO1_IO23"), // reset state
    // ALT6: USDHC2_DATA3
    // ALT7: KPP_COL04
    // ALT8: GPT2_COMPARE3
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO07"),
);

iomuxc_pin!(
    Pin17,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_06, sw_mux_ctl_pad_gpio_ad_b1_06),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_06, sw_pad_ctl_pad_gpio_ad_b1_06),
    // SION
    // ALT0: FLEXSPIB_DATA01
    into_lpi2c: crate::lpi2c::LPI2C3 => (alt1, "ALT1: LPI2C3_SDA"),
    into_lpuart_tx: crate::lpuart::LPUART3 => (alt2, "ALT2: LPUART3_TX"),
    // ALT3: SPDIF_LOCK
    // ALT4: CSI_VSYNC
    into_gpio: crate::gpio::Pin17<()> => (alt5, "ALT5: GPIO1_IO22"), // reset state
    // ALT6: USDHC2_DATA2
    // ALT7: KPP_ROW04
    // ALT8: GPT2_COMPARE2
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO06"),
);

iomuxc_pin!(
    Pin18,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_01, sw_mux_ctl_pad_gpio_ad_b1_01),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_01, sw_pad_ctl_pad_gpio_ad_b1_01),
    // SION
    // ALT0: USB_OTG1_PWR
    into_timer: dev::TMR3 => (alt1, "ALT1: QTIMER3_TIMER1"),
    into_lpuart_rts_b: crate::lpuart::LPUART2 => (alt2, "ALT2: LPUART2_RTS_B"),
    into_lpi2c: crate::lpi2c::LPI2C1 => (alt3, "ALT3: LPI2C1_SDA"),
    // ALT4: CCM_PMIC_READY
    into_gpio: crate::gpio::Pin18<()> => (alt5, "ALT5: GPIO1_IO17"), // reset state
    // ALT6: USDHC1_VSELECT
    // ALT7: KPP_COL07
    // ALT8: ENET2_1588_EVENT0_IN
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO01"),
);

iomuxc_pin!(
    Pin19,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_00, sw_mux_ctl_pad_gpio_ad_b1_00),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_00, sw_pad_ctl_pad_gpio_ad_b1_00),
    // SION
    // ALT0: USB_OTG2_ID
    into_timer: dev::TMR3 => (alt1, "ALT1: QTIMER3_TIMER0"),
    into_lpuart_cts_b: crate::lpuart::LPUART2 => (alt2, "ALT2: LPUART2_CTS_B"),
    into_lpi2c: crate::lpi2c::LPI2C1 => (alt3, "ALT3: LPI2C1_SCL"),
    // ALT4: WDOG1_B
    into_gpio: crate::gpio::Pin19<()> => (alt5, "ALT5: GPIO1_IO16"), // reset state
    // ALT6: USDHC1_WP
    // ALT7: KPP_ROW07
    // ALT8: ENET2_1588_EVENT0_OUT
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO00"),
);

iomuxc_pin!(
    Pin20,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_10, sw_mux_ctl_pad_gpio_ad_b1_10),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_10, sw_pad_ctl_pad_gpio_ad_b1_10),
    // SION
    // ALT0: FLEXSPIA_DATA03
    // ALT1: WDOG1_B
    into_lpuart_tx: crate::lpuart::LPUART2 => (alt2, "ALT2: LPUART8_TX"),
    // ALT3: SAI1_RX_SYNC
    // ALT4: CSI_DATA07
    into_gpio: crate::gpio::Pin20<()> => (alt5, "ALT5: GPIO1_IO26"), // reset state
    // ALT6: USDHC2_WP
    // ALT7: KPP_ROW02
    // ALT8: ENET2_1588_EVENT1_OUT
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO10"),
);

iomuxc_pin!(
    Pin21,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_11, sw_mux_ctl_pad_gpio_ad_b1_11),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_11, sw_pad_ctl_pad_gpio_ad_b1_11),
    // SION
    // ALT0: FLEXSPIA_DATA02
    // ALT1: EWM_OUT_B
    into_lpuart_rx: crate::lpuart::LPUART2 => (alt2, "ALT2: LPUART8_RX"),
    // ALT3: SAI1_RX_BCLK
    // ALT4: CSI_DATA06
    into_gpio: crate::gpio::Pin21<()> => (alt5, "ALT5: GPIO1_IO27"), // reset state
    // ALT6: USDHC2_RESET_B
    // ALT7: KPP_COL02
    // ALT8: ENET2_1588_EVENT1_IN
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO11"),
);

iomuxc_pin!(
    Pin22,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_08, sw_mux_ctl_pad_gpio_ad_b1_08),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_08, sw_pad_ctl_pad_gpio_ad_b1_08),
    // SION
    // ALT0: FLEXSPIA_SS1_B
    // ALT1: FLEXPWM4_PWMA00
    // ALT2: FLEXCAN1_TX
    // ALT3: CCM_PMIC_READY
    // ALT4: CSI_DATA09
    into_gpio: crate::gpio::Pin22<()> => (alt5, "ALT5: GPIO1_IO24"), // reset state
    // ALT6: USDHC2_CMD
    // ALT7: KPP_ROW03
    // ALT8:
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO08"),
);

iomuxc_pin!(
    Pin23,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_09, sw_mux_ctl_pad_gpio_ad_b1_09),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_09, sw_pad_ctl_pad_gpio_ad_b1_09),
    // SION
    // ALT0: FLEXSPIA_DQS
    // ALT1: FLEXPWM4_PWMA01
    // ALT2: FLEXCAN1_RX
    // ALT3: SAI1_MCLK
    // ALT4: CSI_DATA08
    into_gpio: crate::gpio::Pin23<()> => (alt5, "ALT5: GPIO1_IO25"), // reset state
    // ALT6: USDHC2_CLK
    // ALT7: KPP_COL03
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO09"),
);

iomuxc_pin!(
    Pin24,
    (SW_MUX_CTL_PAD_GPIO_AD_B0_12, sw_mux_ctl_pad_gpio_ad_b0_12),
    (SW_PAD_CTL_PAD_GPIO_AD_B0_12, sw_pad_ctl_pad_gpio_ad_b0_12),
    // SION
    into_lpi2c: crate::lpi2c::LPI2C4 => (alt3, "ALT0: LPI2C4_SCL"),
    // ALT1: CCM_PMIC_READY
    into_lpuart_tx: crate::lpuart::LPUART1 => (alt2, "ALT2: LPUART1_TX"),
    // ALT3: WDOG2_WDOG_B
    // ALT4: FLEXPWM1_PWMX02
    into_gpio: crate::gpio::Pin24<()> => (alt5, "ALT5: GPIO1_IO12"), // reset state
    // ALT6: ENET_1588_EVENT1_OUT
    // ALT7: NMI_GLUE_NMI
);

iomuxc_pin!(
    Pin25,
    (SW_MUX_CTL_PAD_GPIO_AD_B0_13, sw_mux_ctl_pad_gpio_ad_b0_13),
    (SW_PAD_CTL_PAD_GPIO_AD_B0_13, sw_pad_ctl_pad_gpio_ad_b0_13),
    // SION
    into_lpi2c: crate::lpi2c::LPI2C4 => (alt3, "ALT0: LPI2C4_SDA"),
    // ALT1: GPT1_CLK
    into_lpuart_rx: crate::lpuart::LPUART1 => (alt2, "ALT2: LPUART1_RX"),
    // ALT3: EWM_OUT_B
    // ALT4: FLEXPWM1_PWMX03
    into_gpio: crate::gpio::Pin25<()> => (alt5, "ALT5: GPIO1_IO13"), // reset state
    // ALT6: ENET_1588_EVENT1_IN
    // ALT7: REF_CLK_24M
);

iomuxc_pin!(
    Pin26,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_14, sw_mux_ctl_pad_gpio_ad_b1_14),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_14, sw_pad_ctl_pad_gpio_ad_b1_14),
    // SION
    // ALT0: FLEXSPIA_SCLK
    // ALT1: ACMP_OUT02
    // ALT2: LPSPI3_SDO
    // ALT3: SAI1_TX_BCLK
    // ALT4: CSI_DATA03
    into_gpio: crate::gpio::Pin26<()> => (alt5, "ALT5: GPIO1_IO30"), // reset state
    // ALT6: USDHC2_DATA6
    // ALT7: KPP_ROW00
    // ALT8: ENET2_1588_EVENT3_OUT
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO14"),
);

iomuxc_pin!(
    Pin27,
    (SW_MUX_CTL_PAD_GPIO_AD_B1_15, sw_mux_ctl_pad_gpio_ad_b1_15),
    (SW_PAD_CTL_PAD_GPIO_AD_B1_15, sw_pad_ctl_pad_gpio_ad_b1_15),
    // SION
    // ALT0: FLEXSPIA_SS0_B
    // ALT1: ACMP_OUT03
    // ALT2: LPSPI3_SCK
    // ALT3: SAI1_TX_SYNC
    // ALT4: CSI_DATA02
    into_gpio: crate::gpio::Pin27<()> => (alt5, "ALT5: GPIO1_IO31"), // reset state
    // ALT6: USDHC2_DATA7
    // ALT7: KPP_COL00
    // ALT8: ENET2_1588_EVENT3_IN
    into_flexio: dev::FLEXIO3 => (alt9, "ALT9: FLEXIO3_FLEXIO15"),
);

iomuxc_pin!(
    Pin28,
    (SW_MUX_CTL_PAD_GPIO_EMC_32, sw_mux_ctl_pad_gpio_emc_32),
    (SW_PAD_CTL_PAD_GPIO_EMC_32, sw_pad_ctl_pad_gpio_emc_32),
    // SION
    // ALT0: SEMC_DATA10
    // ALT1: FLEXPWM3_PWMB01
    into_lpuart_rx: crate::lpuart::LPUART7 => (alt2, "ALT2: LPUART7_RX"),
    // ALT3: CCM_PMIC_RDY
    // ALT4: CSI_DATA21
    into_gpio: crate::gpio::Pin28<()> => (alt5, "ALT5: GPIO3_IO18"), // reset state
    // ALT8: ENET2_TX_EN
);

iomuxc_pin!(
    Pin29,
    (SW_MUX_CTL_PAD_GPIO_EMC_31, sw_mux_ctl_pad_gpio_emc_31),
    (SW_PAD_CTL_PAD_GPIO_EMC_31, sw_pad_ctl_pad_gpio_emc_31),
    // SION
    // ALT0: SEMC_DATA09
    // ALT1: FLEXPWM3_PWMA01
    into_lpuart_tx: crate::lpuart::LPUART7 => (alt2, "ALT2: LPUART7_TX"),
    // ALT3: LPSPI1_PCS1
    // ALT4: CSI_DATA22
    into_gpio: crate::gpio::Pin29<()> => (alt5, "ALT5: GPIO4_IO31"), // reset state
    // ALT8: ENET2_TDATA01
);

iomuxc_pin!(
    Pin30,
    (SW_MUX_CTL_PAD_GPIO_EMC_37, sw_mux_ctl_pad_gpio_emc_37),
    (SW_PAD_CTL_PAD_GPIO_EMC_37, sw_pad_ctl_pad_gpio_emc_37),
    // SION
    // ALT0: SEMC_DATA15
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_IN23"),
    // ALT2: GPT1_COMPARE3
    // ALT3: SAI3_MCLK
    // ALT4: CSI_DATA16
    into_gpio: crate::gpio::Pin30<()> => (alt5, "ALT5: GPIO3_IO23"), // reset state
    // ALT6: USDHC2_WP
    // ALT8: ENET2_RX_EN
    // ALT9: FLEXCAN3_RX
);

iomuxc_pin!(
    Pin31,
    (SW_MUX_CTL_PAD_GPIO_EMC_36, sw_mux_ctl_pad_gpio_emc_36),
    (SW_PAD_CTL_PAD_GPIO_EMC_36, sw_pad_ctl_pad_gpio_emc_36),
    // SION
    // ALT0: SEMC_DATA14
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_IN22"),
    // ALT2: GPT1_COMPARE2
    // ALT3: SAI3_TX_DATA
    // ALT4: CSI_DATA17
    into_gpio: crate::gpio::Pin31<()> => (alt5, "ALT5: GPIO3_IO22"), // reset state
    // ALT6: USDHC1_WP
    // ALT8: ENET2_RDATA01
    // ALT9: FLEXCAN3_TX
);

iomuxc_pin!(
    Pin32,
    (SW_MUX_CTL_PAD_GPIO_B0_12, sw_mux_ctl_pad_gpio_b0_12),
    (SW_PAD_CTL_PAD_GPIO_B0_12, sw_pad_ctl_pad_gpio_b0_12),
    // SION
    // ALT0: LCD_DATA08
    into_xbar: crate::xbar::XBAR1 => (alt1, "ALT1: XBAR1_INOUT10"),
    // ALT2: ARM_TRACE_CLK
    // ALT3: SAI1_TX_DATA01
    into_flexio: dev::FLEXIO2 => (alt4, "ALT4: FLEXIO2_FLEXIO12"),
    into_gpio: crate::gpio::Pin32<()> => (alt5, "ALT5: GPIO2_IO12"), // reset state
    // ALT6: SRC_BOOT_CFG08
    // ALT8: ENET2_TDATA00
);

iomuxc_pin!(
    Pin33,
    (SW_MUX_CTL_PAD_GPIO_EMC_07, sw_mux_ctl_pad_gpio_emc_07),
    (SW_PAD_CTL_PAD_GPIO_EMC_07, sw_pad_ctl_pad_gpio_emc_07),
    // SION
    // ALT0: SEMC_DATA07
    // ALT1: FLEXPWM2_PWMB00
    // ALT2: SAI2_MCLK
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT09"),
    into_flexio: dev::FLEXIO1 => (alt4, "ALT4: FLEXIO1_FLEXIO07"),
    into_gpio: crate::gpio::Pin33<()> => (alt5, "ALT5: GPIO4_IO07"), // reset state
);

iomuxc_pin!(
    Pin34,
    (SW_MUX_CTL_PAD_GPIO_SD_B0_03, sw_mux_ctl_pad_gpio_sd_b0_03),
    (SW_PAD_CTL_PAD_GPIO_SD_B0_03, sw_pad_ctl_pad_gpio_sd_b0_03),
    // SION
    // ALT0: USDHC1_DATA1
    // ALT1: FLEXPWM1_PWMB01
    into_lpuart_rts_b: crate::lpuart::LPUART8 => (alt2, "ALT2: LPUART8_RTS_B"),
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT07"),
    // ALT4: LPSPI1_SDI
    into_gpio: crate::gpio::Pin34<()> => (alt5, "ALT5: GPIO3_IO15"), // reset state
    // ALT8: ENET2_RDATA00
    // ALT9: SEMC_CLK6
);

iomuxc_pin!(
    Pin35,
    (SW_MUX_CTL_PAD_GPIO_SD_B0_02, sw_mux_ctl_pad_gpio_sd_b0_02),
    (SW_PAD_CTL_PAD_GPIO_SD_B0_02, sw_pad_ctl_pad_gpio_sd_b0_02),
    // SION
    // ALT0: USDHC1_DATA0
    // ALT1: FLEXPWM1_PWMA01
    into_lpuart_cts_b: crate::lpuart::LPUART8 => (alt2, "ALT2: LPUART8_CTS_B"),
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT06"),
    // ALT4: LPSPI1_SDO
    into_gpio: crate::gpio::Pin35<()> => (alt5, "ALT5: GPIO3_IO14"), // reset state
    // ALT8: ENET2_RX_ER
    // ALT9: SEMC_CLK5
);

iomuxc_pin!(
    Pin36,
    (SW_MUX_CTL_PAD_GPIO_SD_B0_01, sw_mux_ctl_pad_gpio_sd_b0_01),
    (SW_PAD_CTL_PAD_GPIO_SD_B0_01, sw_pad_ctl_pad_gpio_sd_b0_01),
    // SION
    // ALT0: USDHC1_CLK
    // ALT1: FLEXPWM1_PWMB00
    // ALT2: LPI2C3_SDA
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT05"),
    // ALT4: LPSPI1_PCS0
    into_gpio: crate::gpio::Pin36<()> => (alt5, "ALT5: GPIO3_IO13"), // reset state
    // ALT6: FLEXSPIB_SS1_B
    // ALT8: ENET2_TX_CLK
    // ALT9: ENET2_REF_CLK2
);

iomuxc_pin!(
    Pin37,
    (SW_MUX_CTL_PAD_GPIO_SD_B0_00, sw_mux_ctl_pad_gpio_sd_b0_00),
    (SW_PAD_CTL_PAD_GPIO_SD_B0_00, sw_pad_ctl_pad_gpio_sd_b0_00),
    // SION
    // ALT0: USDHC1_CMD
    // ALT1: FLEXPWM1_PWMA00
    // ALT2: LPI2C3_SCL
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT04"),
    // ALT4: LPSPI1_SCK
    into_gpio: crate::gpio::Pin37<()> => (alt5, "ALT5: GPIO3_IO12"), // reset state
    // ALT6: FLEXSPIA_SS1_B
    // ALT8: ENET2_TX_EN
    // ALT9: SEMC_DQS4
);

iomuxc_pin!(
    Pin38,
    (SW_MUX_CTL_PAD_GPIO_SD_B0_05, sw_mux_ctl_pad_gpio_sd_b0_05),
    (SW_PAD_CTL_PAD_GPIO_SD_B0_05, sw_pad_ctl_pad_gpio_sd_b0_05),
    // SION
    // ALT0: USDHC1_DATA3
    // ALT1: FLEXPWM1_PWMB02
    into_lpuart_rx: crate::lpuart::LPUART8 => (alt2, "ALT2: LPUART8_RX"),
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT09"),
    // ALT4: FLEXSPIB_DQS
    into_gpio: crate::gpio::Pin38<()> => (alt5, "ALT5: GPIO3_IO17"), // reset state
    // ALT6: CCM_CLKO2
    // ALT8: ENET2_RX_EN
);

iomuxc_pin!(
    Pin39,
    (SW_MUX_CTL_PAD_GPIO_SD_B0_04, sw_mux_ctl_pad_gpio_sd_b0_04),
    (SW_PAD_CTL_PAD_GPIO_SD_B0_04, sw_pad_ctl_pad_gpio_sd_b0_04),
    // SION
    // ALT0: USDHC1_DATA2
    // ALT1: FLEXPWM1_PWMA02
    into_lpuart_tx: crate::lpuart::LPUART8 => (alt2, "ALT2: LPUART8_TX"),
    into_xbar: crate::xbar::XBAR1 => (alt3, "ALT3: XBAR1_INOUT08"),
    // ALT4: FLEXSPIB_SS0_B
    into_gpio: crate::gpio::Pin39<()> => (alt5, "ALT5: GPIO3_IO16"), // reset state
    // ALT6: CCM_CLKO1
    // ALT8: ENET2_RDATA01
);

#![no_std]
#![feature(trait_alias)]

pub extern crate cortex_m as platform;
pub extern crate embedded_hal as hal;
pub extern crate mimxrt1062 as dev;

#[cfg(feature = "rt")]
pub extern crate cortex_m_rt;

extern crate nb;
extern crate smart_default;
extern crate typenum;

#[cfg(feature = "rt")]
pub mod rt;

pub use dev::{CorePeripherals, Peripherals};

#[macro_use]
mod generic;

pub mod ccm;
pub mod dma;
pub mod enc;
pub mod flexio;
pub mod gpio;
pub mod iomuxc;
pub mod lpi2c;
pub mod lpuart;
pub mod prelude;
pub mod qtimer;
pub mod snvs;
pub mod xbar;

// TODO: optional (feature-controlled) runtime checks for
//       PinN/TimerN/… to compare against FLEXIOn_PARAM (and possibly
//       panic if invalid to avoid sprinkling code with .unwrap())

/// FlexIO pin. Actual number of valid pins might be lower (see FLEXIOn_PARAM[PIN])
#[derive(Copy, Clone)]
#[repr(u8)]
pub enum PinNumber {
    Pin0 = 0,
    Pin1,
    Pin2,
    Pin3,
    Pin4,
    Pin5,
    Pin6,
    Pin7,
    Pin8,
    Pin9,
    Pin10,
    Pin11,
    Pin12,
    Pin13,
    Pin14,
    Pin15,
    Pin16,
    Pin17,
    Pin18,
    Pin19,
    Pin20,
    Pin21,
    Pin22,
    Pin23,
    Pin24,
    Pin25,
    Pin26,
    Pin27,
    Pin28,
    Pin29,
    Pin30,
    Pin31,
    Pin32,
}
pub use PinNumber::*;

/// FlexIO timer. Actual number of valid timers might be lower (see FLEXIOn_PARAM[TIMER])
#[derive(Copy, Clone)]
#[repr(u8)]
pub enum TimerNumber {
    Timer0 = 0,
    Timer1,
    Timer2,
    Timer3,
    Timer4,
    Timer5,
    Timer6,
    Timer7,
    Timer8,
    Timer9,
    Timer10,
    Timer11,
    Timer12,
    Timer13,
    Timer14,
    Timer15,
    Timer16,
}
pub use TimerNumber::*;

/// FlexIO shifter. Actual number of valid shifters might be lower (see FLEXIOn_PARAM[SHIFTER])
#[derive(Copy, Clone)]
#[repr(u8)]
pub enum ShifterNumber {
    Shifter0 = 0,
    Shifter1,
    Shifter2,
    Shifter3,
    Shifter4,
    Shifter5,
    Shifter6,
    Shifter7,
    Shifter8,
    Shifter9,
    Shifter10,
    Shifter11,
    Shifter12,
    Shifter13,
    Shifter14,
    Shifter15,
    Shifter16,
}
pub use ShifterNumber::*;

/// FlexIO external trigger. Actual number of valid triggers might be lower (see FLEXIOn_PARAM[TRIGGER])
#[derive(Copy, Clone)]
#[repr(u8)]
pub enum TriggerNumber {
    // FIXME: There are 6 bits for trigger in TIMERn_TIMCTL[TRGSEL],
    //        so there should be 64 variants here?  And are external
    //        triggers even implemented in imxrt1062 / Teensy4
    Trigger0 = 0,
    Trigger1,
    Trigger2,
    Trigger3,
    Trigger4,
    Trigger5,
    Trigger6,
    Trigger7,
    Trigger8,
    Trigger9,
    Trigger10,
    Trigger11,
    Trigger12,
    Trigger13,
    Trigger14,
    Trigger15,
    Trigger16,
    Trigger17,
    Trigger18,
    Trigger19,
    Trigger20,
    Trigger21,
    Trigger22,
    Trigger23,
    Trigger24,
    Trigger25,
    Trigger26,
    Trigger27,
    Trigger28,
    Trigger29,
    Trigger30,
    Trigger31,
    Trigger32,
}
pub use TriggerNumber::*;

#[derive(Copy, Clone)]
pub enum Polarity {
    ActiveHigh,
    ActiveLow,
}

// The pin configuration for each timer and shifter can be configured
// to use any FlexIO pin with either polarity. Each timer and shifter
// can be configured as an input, output data, output enable or
// bidirectional output. A pin configured for output enable can be
// used as an open drain (with inverted polarity, since the output
// enable assertion would cause logic zero to be output on the pin) or
// to control the enable on the bidirectional output. Any timer or
// shifter could be configured to control the output enable for a pin
// where the bidirectional output data is driven by another timer or
// shifter.

#[derive(Copy, Clone)]
pub enum PinSelect {
    /// Timer pin output disabled
    Disabled,

    /// Timer pin open drain or bidirectional output enable
    OpenDrain(PinNumber, Polarity), // is polarity needed for OE (or OD)? Can we make an alias here?

    /// Timer pin bidirectional output data
    BidirectionalOutputData(PinNumber, Polarity),

    /// Timer pin output
    Output(PinNumber, Polarity),
}

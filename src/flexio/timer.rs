use core::default::Default;

use crate::dev;
use crate::generic::*;

use smart_default::SmartDefault;

use dev::flexio1::{timcfg, timcmp, timctl};

use super::enums::*;

#[derive(Copy, Clone)]
pub enum Mode {
    /// Disabled
    Disabled,

    /// 8-bit Baud Counter Mode (bits, divider)
    Dual8BitBaud(u8, u8),

    /// 8-bit High PWM Mode (high period, low period)
    Dual8BitPWM(u8, u8),

    /// 16-bit Counter Mode (raw TIMCMP_CMP value, as it is treated differently depending on mode)
    Single16bitCounter(u16),
}

impl Mode {
    // TODO: const fn?
    fn timcmp_cmp(self) -> u16 {
        match self {
            Self::Disabled => 0,

            // TODO: check for validity of bits/divider (either via types, or in runtime)
            Self::Dual8BitBaud(bits, divider) => {
                // The lower 8-bits configure the baud rate divider equal to (CMP[7:0] + 1) * 2.
                ((divider as u16 / 2) - 1)

                // The upper 8-bits configure the number of bits in each word equal to (CMP[15:8] + 1) / 2.
                    | ((bits as u16 * 2) - 1) << 8
            }

            // the lower 8-bits configure the high period of the
            // output to (CMP[7:0] + 1) and the upper 8-bits configure
            // the low period of the output to (CMP[15:8] + 1).
            Self::Dual8BitPWM(high, low) => (low as u16 - 1) << 8 | (high as u16 - 1),

            // The compare value can be used to generate the baud rate
            // divider (if shift clock source is timer output) to
            // equal (CMP[15:0] + 1) * 2. When the shift clock source
            // is a pin or trigger input, the compare register is used
            // to set the number of bits in each word equal to
            // (CMP[15:0] + 1) / 2.
            //
            // TODO: meaningful calculation like above? It would need
            //       to depend on shifter config somehow.
            Self::Single16bitCounter(cmp) => cmp,
        }
    }
}

impl Apply<Mode> for timcmp::W {
    #[inline(always)]
    fn apply(&mut self, tm: Mode) -> &mut Self {
        unsafe { self.cmp().bits(tm.timcmp_cmp()) }
    }
}

impl_into_variant!(
    Mode,
    timctl::TIMOD_A,
    Disabled => TIMOD_0,
    Dual8BitBaud(_, _) => TIMOD_1,
    Dual8BitPWM(_, _) => TIMOD_2,
    Single16bitCounter(_) => TIMOD_3,
);

impl_apply_as_field_variant!(Mode, timctl, timod);

/// Timer trigger select
#[derive(Copy, Clone)]
pub enum Trigger {
    /// External trigger
    External(TriggerNumber, Polarity),

    /// Pin input
    Pin(PinNumber, Polarity),

    /// Shifter status
    Shifter(ShifterNumber, Polarity),

    /// Timer trigger output
    Timer(TimerNumber, Polarity),
}

impl Trigger {
    fn trgsel(self) -> u8 {
        match self {
            Self::External(trigger, _) => trigger as u8, // 6 bits? trigger=0 in param
            Self::Pin(pin, _) => (pin as u8) << 1,
            Self::Shifter(shifter, _) => (shifter as u8) << 2 | 1, // 4 bits
            Self::Timer(timer, _) => (timer as u8) << 2 | 3,       // 4 bits
        }
    }
}

impl_into_variant!(
    Polarity,
    timctl::TRGPOL_A,
    ActiveHigh => TRGPOL_0,
    ActiveLow => TRGPOL_1,
);

impl From<Trigger> for timctl::TRGPOL_A {
    #[inline(always)]
    fn from(trigger: Trigger) -> Self {
        match trigger {
            Trigger::External(_, p)
            | Trigger::Pin(_, p)
            | Trigger::Shifter(_, p)
            | Trigger::Timer(_, p) => p,
        }
        .into()
    }
}

impl_into_variant!(
    Trigger,
    timctl::TRGSRC_A,
    External(_, _) => TRGSRC_0,
    _ => TRGSRC_1,
);

impl Apply<Trigger> for timctl::W {
    #[inline(always)]
    fn apply(&mut self, trigger: Trigger) -> &mut Self {
        unsafe { self.trgsel().bits(trigger.trgsel()) }
            .trgsrc()
            .variant(trigger.into())
            .trgpol()
            .variant(trigger.into())
    }
}

impl_into_variant!(
    PinSelect,
    timctl::PINCFG_A,
    Disabled => PINCFG_0,
    OpenDrain(_, _) => PINCFG_1,
    BidirectionalOutputData(_, _) => PINCFG_2,
    Output(_, _) => PINCFG_3,
);

impl_into_variant!(
    Polarity,
    timctl::PINPOL_A,
    ActiveHigh => PINPOL_0,
    ActiveLow => PINPOL_1,
);

impl From<PinSelect> for timctl::PINPOL_A {
    #[inline(always)]
    fn from(pin: PinSelect) -> Self {
        match pin {
            PinSelect::Disabled => Polarity::ActiveHigh,
            PinSelect::OpenDrain(_, p)
            | PinSelect::BidirectionalOutputData(_, p)
            | PinSelect::Output(_, p) => p,
        }
        .into()
    }
}

impl Apply<PinSelect> for timctl::W {
    #[inline(always)]
    fn apply(&mut self, pin: PinSelect) -> &mut Self {
        let pin_bits = match pin {
            PinSelect::Disabled => Pin0,
            PinSelect::OpenDrain(p, _)
            | PinSelect::BidirectionalOutputData(p, _)
            | PinSelect::Output(p, _) => p,
        } as u8;
        unsafe { self.pinsel().bits(pin_bits) }
            .pincfg()
            .variant(pin.into())
            .pinpol()
            .variant(pin.into())
    }
}

/// Configures the initial state of the Timer Output and whether it is
/// affected by the Timer reset.
#[derive(Copy, Clone, SmartDefault)]
pub enum Output {
    /// Timer output is logic one when enabled and is not affected by timer reset
    #[default]
    HighWhenEnabled,

    /// Timer output is logic zero when enabled and is not affected by timer reset
    LowWhenEnabled,

    /// Timer output is logic one when enabled and on timer reset
    HighWhenEnabledAndReset,

    /// Timer output is logic zero when enabled and on timer reset
    LowWhenEnabledAndReset,
}

impl_into_variant!(
    Output,
    timcfg::TIMOUT_A,
    HighWhenEnabled => TIMOUT_0,
    LowWhenEnabled => TIMOUT_1,
    HighWhenEnabledAndReset => TIMOUT_2,
    LowWhenEnabledAndReset => TIMOUT_3,
);

impl_apply_as_field_variant!(Output, timcfg, timout);

/// Configures the source of the Timer decrement and the source of the Shift clock.
#[derive(Copy, Clone, SmartDefault)]
pub enum Decrement {
    /// Decrement counter on FlexIO clock, Shift clock equals Timer output.
    #[default]
    Clock,

    /// Decrement counter on Trigger input (both edges), Shift clock equals Timer output.
    Trigger,

    /// Decrement counter on Pin input (both edges), Shift clock equals Pin input.
    PinShift,

    /// Decrement counter on Trigger input (both edges), Shift clock equals Trigger input.
    TriggerShift,
}

impl_into_variant!(
    Decrement,
    timcfg::TIMDEC_A,
    Clock => TIMDEC_0,
    Trigger => TIMDEC_1,
    PinShift => TIMDEC_2,
    TriggerShift => TIMDEC_3,
);

impl_apply_as_field_variant!(Decrement, timcfg, timdec);

/// Configures the condition that causes the timer counter (and
/// optionally the timer output) to be reset. In 8-bit counter mode,
/// the timer reset will only reset the lower 8-bits that configure
/// the baud rate. In all other modes, the timer reset will reset the
/// full 16-bits of the counter.
#[derive(Copy, Clone, SmartDefault)]
pub enum Reset {
    /// Timer never reset
    #[default]
    Never,

    /// Timer reset on Timer Pin equal to Timer Output
    PinEqualsOutput,

    /// Timer reset on Timer Trigger equal to Timer Output
    TriggerEqualsOutput,

    /// Timer reset on Timer Pin rising edge
    PinRisingEdge,

    /// Timer reset on Trigger rising edge
    TriggerRisingEdge,

    /// Timer reset on Trigger rising or falling edge
    TriggerAnyEdge,
}

impl_into_variant!(
    Reset,
    timcfg::TIMRST_A,
    Never => TIMRST_0,
    PinEqualsOutput => TIMRST_2,
    TriggerEqualsOutput => TIMRST_3,
    PinRisingEdge => TIMRST_4,
    TriggerRisingEdge => TIMRST_6,
    TriggerAnyEdge => TIMRST_7,
);

impl_apply_as_field_variant!(Reset, timcfg, timrst);

/// Configures the condition that causes the Timer to be disabled and stop decrementing.
#[derive(Copy, Clone, SmartDefault)]
pub enum Disable {
    /// Timer never disabled
    #[default]
    Never,

    /// Timer disabled on Timer N-1 disable
    PreviousTimer,

    /// Timer disabled on Timer compare
    Compare,

    /// Timer disabled on Timer compare and Trigger Low
    CompareTriggerLow,

    /// Timer disabled on Pin rising or falling edge
    PinEdge,

    /// Timer disabled on Pin rising or falling edge provided Trigger is high
    PinEdgeTriggerHigh,

    /// Timer disabled on Trigger falling edge
    TriggerFalling,
}

impl_into_variant!(
    Disable,
    timcfg::TIMDIS_A,
    Never => TIMDIS_0,
    PreviousTimer => TIMDIS_1,
    Compare => TIMDIS_2,
    CompareTriggerLow => TIMDIS_3,
    PinEdge => TIMDIS_4,
    PinEdgeTriggerHigh => TIMDIS_5,
    TriggerFalling => TIMDIS_6,
);

impl_apply_as_field_variant!(Disable, timcfg, timdis);

/// Timer Enable
/// Configures the condition that causes the Timer to be enabled and start decrementing.
#[derive(Copy, Clone, SmartDefault)]
pub enum Enable {
    /// Timer always enabled
    #[default]
    Always,

    /// Timer enabled on Timer N-1 enable
    PreviousTimer,

    /// Timer enabled on Trigger high
    Trigger,

    /// Timer enabled on Trigger high and Pin high
    TriggerAndPin,

    /// Timer enabled on Pin rising edge
    PinRising,

    /// Timer enabled on Pin rising edge and Trigger high
    PinRisingTriggerHigh,

    /// Timer enabled on Trigger rising edge
    TriggerRising,

    /// Timer enabled on Trigger rising or falling edge
    TriggerEdge,
}

impl_into_variant!(
    Enable,
    timcfg::TIMENA_A,
    Always => TIMENA_0,
    PreviousTimer => TIMENA_1,
    Trigger => TIMENA_2,
    TriggerAndPin => TIMENA_3,
    PinRising => TIMENA_4,
    PinRisingTriggerHigh => TIMENA_5,
    TriggerRising => TIMENA_6,
    TriggerEdge => TIMENA_7,
);

impl_apply_as_field_variant!(Enable, timcfg, timena);

/// The stop bit can be added on a timer compare (between each word) or on a timer disable. When stop bit
/// is enabled, configured shifters will output the contents of the stop bit when the timer is disabled. When
/// stop bit is enabled on timer disable, the timer remains disabled until the next rising edge of the shift clock.
/// If configured for both timer compare and timer disable, only one stop bit is inserted on timer disable.
#[derive(Copy, Clone, SmartDefault)]
pub enum StopBit {
    /// Stop bit disabled
    #[default]
    Disabled,

    /// Stop bit is enabled on timer compare
    OnCompare,

    /// Stop bit is enabled on timer disable
    OnDisable,

    /// Stop bit is enabled on timer compare and timer disable
    OnCompareAndDisable,
}

impl_into_variant!(
    StopBit,
    timcfg::TSTOP_A,
    Disabled => TSTOP_0,
    OnCompare => TSTOP_1,
    OnDisable => TSTOP_2,
    OnCompareAndDisable => TSTOP_3,
);

impl_apply_as_field_variant!(StopBit, timcfg, tstop);

/// When start bit is enabled, configured shifters will output the contents of the start bit when the timer is
/// enabled and the timer counter will reload from the compare register on the first rising edge of the shift
/// clock.
#[derive(Copy, Clone, SmartDefault)]
pub enum StartBit {
    /// 0b - Start bit disabled
    #[default]
    Disabled,

    /// 1b - Start bit enabled
    Enabled,
}

impl_into_variant!(
    StartBit,
    timcfg::TSTART_A,
    Disabled => TSTART_0,
    Enabled => TSTART_1,
);

impl_apply_as_field_variant!(StartBit, timcfg, tstart);

#[derive(Default)]
pub struct Config {
    pub output: Output,
    pub decrement: Decrement,
    pub reset: Reset,
    pub disable: Disable,
    pub enable: Enable,
    pub stop_bit: StopBit,
    pub start_bit: StartBit,
}

impl Apply<&Config> for timcfg::W {
    #[inline(always)]
    fn apply(&mut self, cfg: &Config) -> &mut Self {
        self.apply(cfg.output)
            .apply(cfg.decrement)
            .apply(cfg.reset)
            .apply(cfg.disable)
            .apply(cfg.enable)
            .apply(cfg.stop_bit)
            .apply(cfg.start_bit)
    }
}

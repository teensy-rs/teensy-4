// TODO: SHIFTCFG[PWIDTH]
// TODO: state & logic mode supposedly uses some of the SHIFTCFG
//       fields differently? It might need a separate API, but let's
//       just require low level access now.

use core::default::Default;

use crate::dev;
use crate::generic::*;

use smart_default::SmartDefault;

use dev::flexio1::{shiftcfg, shiftctl};

use super::enums::*;

#[derive(Copy, Clone, SmartDefault)]
/// Configures the mode of the Shifter.
pub enum Mode {
    /// Disabled.
    #[default]
    Disabled,

    /// Receive mode. Captures the current Shifter content into the SHIFTBUF on expiration of the Timer.
    Receive,

    /// Transmit mode. Load SHIFTBUF contents into the Shifter on expiration of the Timer.
    Transmit,

    /// Match Store mode. Shifter data is compared to SHIFTBUF content on expiration of the Timer.
    MatchStore,

    /// Match Continuous mode. Shifter data is continuously compared to SHIFTBUF contents.
    MatchContinuous,

    /// State mode. SHIFTBUF contents are used for storing programmable state attributes.
    State,

    /// Logic mode. SHIFTBUF contents are used for implementing programmable logic look up table.
    Logic,
}

impl_into_variant!(
    Mode,
    shiftctl::SMOD_A,
    Disabled => SMOD_0,
    Receive => SMOD_1,
    Transmit => SMOD_2,
    MatchStore => SMOD_4,
    MatchContinuous => SMOD_5,
    State => SMOD_6,
    Logic => SMOD_7,
);

impl_apply_as_field_variant!(Mode, shiftctl, smod);

impl_into_variant!(
    PinSelect,
    shiftctl::PINCFG_A,
    Disabled => PINCFG_0,
    OpenDrain(_, _) => PINCFG_1,
    BidirectionalOutputData(_, _) => PINCFG_2,
    Output(_, _) => PINCFG_3,
);

impl_into_variant!(
    Polarity,
    shiftctl::PINPOL_A,
    ActiveHigh => PINPOL_0,
    ActiveLow => PINPOL_1,
);

impl From<PinSelect> for shiftctl::PINPOL_A {
    #[inline(always)]
    fn from(pin: PinSelect) -> Self {
        match pin {
            PinSelect::Disabled => Polarity::ActiveHigh,
            PinSelect::OpenDrain(_, p)
            | PinSelect::BidirectionalOutputData(_, p)
            | PinSelect::Output(_, p) => p,
        }
        .into()
    }
}

impl Apply<PinSelect> for shiftctl::W {
    #[inline(always)]
    fn apply(&mut self, pin: PinSelect) -> &mut Self {
        let pin_bits = match pin {
            PinSelect::Disabled => Pin0,
            PinSelect::OpenDrain(p, _)
            | PinSelect::BidirectionalOutputData(p, _)
            | PinSelect::Output(p, _) => p,
        } as u8;
        unsafe { self.pinsel().bits(pin_bits) }
            .pincfg()
            .variant(pin.into())
            .pinpol()
            .variant(pin.into())
    }
}

/// Selects which Timer is used for controlling the logic/shift register and generating the Shift clock.
#[derive(Copy, Clone)]
pub enum TimerSelect {
    /// Shift on rising edge of selected timer
    Rising(TimerNumber),

    /// Shift on falling edge of selected timer
    Falling(TimerNumber),
}

impl_into_variant!(
    TimerSelect,
    shiftctl::TIMPOL_A,
    Rising(_) => TIMPOL_0,
    Falling(_) => TIMPOL_1,
);

impl Apply<TimerSelect> for shiftctl::W {
    #[inline(always)]
    fn apply(&mut self, timer: TimerSelect) -> &mut Self {
        let n = match timer {
            TimerSelect::Rising(t) | TimerSelect::Falling(t) => t as u8,
        };
        unsafe { self.timsel().bits(n) }
            .timpol()
            .variant(timer.into())
    }
}

/// Selects the input source for the shifter.
#[derive(Copy, Clone, SmartDefault)]
pub enum InputSource {
    /// Pin
    #[default]
    Pin,

    /// Shifter N+1 Output
    NextShifter,
}

impl_into_variant!(
    InputSource,
    shiftcfg::INSRC_A,
    Pin => INSRC_0,
    NextShifter => INSRC_1,
);

impl_apply_as_field_variant!(InputSource, shiftcfg, insrc);

/// Shifter Stop bit
///
/// - For SMOD=Transmit, this field allows automatic stop bit
///   insertion if the selected timer has also enabled a stop bit.
/// - For SMOD=Receive or Match Store, this field allows automatic
///   stop bit checking if the selected timer has also enabled a stop
///   bit.
/// - For SMOD=State, this field is used to disable state outputs. See
///   'State Mode' section for more detail.
/// - For SMOD=Logic, this field is used to mask logic pin inputs. See
///   'Logic Mode' section for more detail.
#[derive(Copy, Clone, SmartDefault)]
pub enum StopBit {
    /// 00b - Stop bit disabled for transmitter/receiver/match store
    #[default]
    Disabled,

    // TODO: 01b - Reserved for transmitter/receiver/match store (i.e. supposedly used in state & logic mode)
    /// 10b - Transmitter outputs stop bit value 0 on store, receiver/match store sets error flag if stop bit is not 0
    Zero,

    /// 11b - Transmitter outputs stop bit value 1 on store, receiver/match store sets error flag if stop bit is not 1
    One,
}

impl_into_variant!(
    StopBit,
    shiftcfg::SSTOP_A,
    Disabled => SSTOP_0,
    Zero => SSTOP_2,
    One => SSTOP_3,
);

impl_apply_as_field_variant!(StopBit, shiftcfg, sstop);

/// Shifter Start bit
///
/// - For SMOD=Transmit, this field allows automatic start bit
///   insertion if the selected timer has also enabled a start bit.
/// - For SMOD=Receive or Match Store, this field allows automatic
///   start bit checking if the selected timer has also enabled a
///   start bit.
/// - For SMOD=State, this field is used to disable state outputs. See
///   'State Mode' section for more detail.
/// - For SMOD=Logic, this field is used to mask logic pin inputs. See
///   'Logic Mode' section for more detail.
#[derive(Copy, Clone, SmartDefault)]
pub enum StartBit {
    /// Start bit disabled for transmitter/receiver/match store,
    /// transmitter loads data on enable
    #[default]
    Disabled,

    /// Start bit disabled for transmitter/receiver/match store,
    /// transmitter loads data on first shift
    DelayLoad,

    /// Transmitter outputs start bit value 0 before loading data on
    /// first shift, receiver/match store sets error flag if start bit
    /// is not 0
    Zero,

    /// Transmitter outputs start bit value 1 before loading data on
    /// first shift, receiver/match store sets error flag if start bit
    /// is not 1
    One,
}

impl_into_variant!(
    StartBit,
    shiftcfg::SSTART_A,
    Disabled => SSTART_0,
    DelayLoad => SSTART_1,
    Zero => SSTART_2,
    One => SSTART_3,
);

impl_apply_as_field_variant!(StartBit, shiftcfg, sstart);

#[derive(Default)]
pub struct Config {
    // TODO: pwidth
    pub input_source: InputSource,
    pub stop_bit: StopBit,
    pub start_bit: StartBit,
}

impl Apply<&Config> for shiftcfg::W {
    #[inline(always)]
    fn apply(&mut self, cfg: &Config) -> &mut Self {
        self.apply(cfg.input_source)
            .apply(cfg.stop_bit)
            .apply(cfg.start_bit)
    }
}

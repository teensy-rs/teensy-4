use core::ops::Deref;

use crate::dev;
use crate::generic::*;

impl _Function for dev::FLEXIO1 {}
impl _Function for dev::FLEXIO2 {}
impl _Function for dev::FLEXIO3 {}

// Note to self:
// FLEXIO1 shifter=8 timer=8 pin=16 trigger=2
// FLEXIO2 shifter=8 timer=8 pin=32 trigger=2
// FLEXIO3 shifter=8 timer=8 pin=32 trigger=2

pub mod enums;
pub mod shifter;
pub mod timer;

pub use enums::*;

// compat
pub use shifter::{
    Config as ShifterConfig, InputSource as ShifterInputSource, Mode as ShifterMode,
    StartBit as ShifterStartBit, StopBit as ShifterStopBit, TimerSelect,
};
pub use timer::{
    Config as TimerConfig, Decrement as TimerDecrement, Disable as TimerDisable,
    Enable as TimerEnable, Mode as TimerMode, Output as TimerOutput, Reset as TimerReset,
    StartBit as TimerStartBit, StopBit as TimerStopBit, Trigger as TimerTrigger,
};

use dev::flexio1::RegisterBlock;

pub trait FlexioExt {
    fn configure_timer(
        &mut self,
        timer: TimerNumber,
        mode: timer::Mode,
        trigger: timer::Trigger,
        pin: PinSelect,
        config: &timer::Config,
    );
}

impl<T> FlexioExt for T
where
    T: Deref<Target = RegisterBlock>,
{
    #[inline(always)]
    fn configure_timer(
        &mut self,
        timer: TimerNumber,
        mode: timer::Mode,
        trigger: timer::Trigger,
        pin: PinSelect,
        config: &timer::Config,
    ) {
        self.timctl[timer as usize].reset(); // disable timer
        self.timcmp[timer as usize].write(|w| w.apply(mode));
        self.timcfg[timer as usize].write(|w| w.apply(config));
        self.timctl[timer as usize].write(|w| w.apply(mode).apply(pin).apply(trigger));
    }
}

ccm_enable!(dev::FLEXIO1, ccgr5, cg1);
ccm_enable!(dev::FLEXIO2, ccgr3, cg0);
ccm_enable!(dev::FLEXIO3, ccgr7, cg6);

dmamux_source!(dev::FLEXIO1, 0);
dmamux_source!(dev::FLEXIO2, 1);

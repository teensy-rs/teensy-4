use crate::dev;
use crate::generic::*;
use core::ops::Deref;

pub use dev::enc1::RegisterBlock;

ccm_enable!(dev::ENC1, ccgr4, cg12);
ccm_enable!(dev::ENC2, ccgr4, cg13);
ccm_enable!(dev::ENC3, ccgr4, cg14);
ccm_enable!(dev::ENC4, ccgr4, cg15);

pub trait EncExt {
    /// Clears all interrupt flags
    fn clear_interrupt(&mut self);

    /// Get value of POS register
    fn get_pos(&self) -> u32;

    /// Set POS register
    fn set_pos(&mut self, val: u32);

    /// Get value of POSH (position hold) register
    fn get_posh(&self) -> u32;

    /// Get value of INIT register
    fn get_init(&self) -> u32;

    /// Set value of INIT register
    fn set_init(&mut self, val: u32);

    /// Get value of MOD register
    fn get_mod(&self) -> u32;

    /// Set value of MOD register
    fn set_mod(&mut self, val: u32);

    /// Get value of COMP register
    fn get_comp(&self) -> u32;

    /// Set value of COMP register
    fn set_comp(&mut self, val: u32);

    /// Get value of WTR register
    fn get_wtr(&self) -> u16;

    /// Set value of WTR register
    fn set_wtr(&mut self, val: u16);

    /// Get value of POSD register
    fn get_posd(&self) -> u16;

    /// Set value of POSD register
    fn set_posd(&mut self, val: u16);

    /// Get value of POSDH (POSD hold) register
    fn get_posdh(&self) -> u16;

    /// Get value of REV register
    fn get_rev(&self) -> u16;

    /// Set value of REV register
    fn set_rev(&mut self, val: u16);

    /// Get value of REVH (REV hold) register
    fn get_revh(&self) -> u16;
}

macro_rules! two_word_register {
    ($getter:ident, (), $upper_half:ident, $lower_half:ident) => {
        #[inline]
        fn $getter(&self) -> u32 {
            ((self.$upper_half.read().bits() as u32) << 16) | self.$lower_half.read().bits() as u32
        }
    };

    ($getter:ident, $setter:ident, $upper_half:ident, $lower_half:ident) => {
        two_word_register!($getter, (), $upper_half, $lower_half);

        #[inline]
        fn $setter(&mut self, val: u32) {
            self.$upper_half.write(|w| unsafe { w.bits((val >> 16) as u16) });
            self.$lower_half.write(|w| unsafe { w.bits((val & 0xFFFF) as u16) });
        }
    }
}

macro_rules! single_word_register {
    ($getter:ident, (), $register:ident) => {
        #[inline]
        fn $getter(&self) -> u16 {
            self.$register.read().bits()
        }
    };

    ($getter:ident, $setter:ident, $register:ident) => {
        single_word_register!($getter, (), $register);

        #[inline]
        fn $setter(&mut self, val: u16) {
            self.$register.write(|w| unsafe { w.bits(val) });
        }
    }
}

impl<T> EncExt for T
where
    T: Deref<Target = RegisterBlock>,
{
    two_word_register!(get_pos, set_pos, upos, lpos);
    two_word_register!(get_posh, (), uposh, lposh);
    two_word_register!(get_init, set_init, uinit, linit);
    two_word_register!(get_mod, set_mod, umod, lmod);
    two_word_register!(get_comp, set_comp, ucomp, lcomp);
    single_word_register!(get_wtr, set_wtr, wtr);
    single_word_register!(get_posd, set_posd, posd);
    single_word_register!(get_posdh, (), posdh);
    single_word_register!(get_rev, set_rev, rev);
    single_word_register!(get_revh, (), revh);

    #[inline]
    fn clear_interrupt(&mut self) {
        self.ctrl.modify(|_r, w| {
            w.hirq()
                .set_bit()
                .xirq()
                .set_bit()
                .dirq()
                .set_bit()
                .cmpirq()
                .set_bit()
        });
        self.ctrl2
            .modify(|_r, w| w.ruirq().set_bit().roirq().set_bit());
        crate::platform::asm::dsb(); // Make sure MCU got the hint
    }
}

pub use crate::dev::SNVS;

// FIXME: we don't care about y2k37 problem and use 32-bit UNIX timestamps

/// If LPRTC is unset, reset to Jan 1, 2019
pub const DEFAULT_CLOCK: u32 = 1574167251; //1546300800;

pub trait SnvsExt {
    /// Initialize HPRTC: if LPRTC is set, initialize from it and
    /// return true; otherwise, set to default without touching LPRTC
    /// and return false.
    fn init_rtc(&mut self) -> bool;

    /// Returns true if LPRTC is set
    fn is_rtc_set(&self) -> bool;

    /// Sets LPRTC and resets HPRTC
    fn set_rtc(&mut self, timestamp: u32);

    /// Returns RTC timestamp (seconds, subsecond ticks (32768Hz))
    fn rtc_timestamp(&self) -> (u32, u32);
}

impl SnvsExt for SNVS {
    fn is_rtc_set(&self) -> bool {
        self.lpcr.read().srtc_env().bit_is_set()
    }

    fn init_rtc(&mut self) -> bool {
        let isset = self.is_rtc_set();
        if isset {
            // set from LPRTC
            self.hpcr
                .modify(|_r, w| w.rtc_en().set_bit().hp_ts().set_bit());
        } else {
            // set to default
            self.hprtclr
                .write(|w| unsafe { w.bits(DEFAULT_CLOCK << 15) });
            self.hprtcmr
                .write(|w| unsafe { w.bits(DEFAULT_CLOCK >> 17) });
            self.hpcr.modify(|_r, w| w.rtc_en().set_bit());
        }

        // wait until HP RTC catches up
        while !self.hpcr.read().rtc_en().bit_is_set() {}

        return isset;
    }

    fn set_rtc(&mut self, timestamp: u32) {
        self.lpcr.modify(|_r, w| w.srtc_env().clear_bit());
        while self.lpcr.read().srtc_env().bit_is_set() {}
        self.lpsrtclr.write(|w| unsafe { w.bits(timestamp << 15) });
        self.lpsrtcmr.write(|w| unsafe { w.bits(timestamp >> 17) });
        self.lpcr.modify(|_r, w| w.srtc_env().set_bit());
        while !self.lpcr.read().srtc_env().bit_is_set() {}

        self.hpcr
            .modify(|_r, w| w.rtc_en().clear_bit().hp_ts().clear_bit());
        while self.hpcr.read().rtc_en().bit_is_set() {}
        while self.hpcr.read().hp_ts().bit_is_set() {}
        self.hpcr
            .modify(|_r, w| w.rtc_en().set_bit().hp_ts().set_bit());
        while !self.hpcr.read().rtc_en().bit_is_set() {}
    }

    fn rtc_timestamp(&self) -> (u32, u32) {
        // TODO: panic if looped more than three times?
        loop {
            let h1 = self.hprtcmr.read().bits();
            let l1 = self.hprtclr.read().bits();
            let h2 = self.hprtcmr.read().bits();
            let l2 = self.hprtclr.read().bits();
            if h1 == h2 && l1 == l2 {
                return (
                    h1 << 17 | l1 >> 15, // seconds
                    l1 & (1 << 15 - 1),  // subsecond ticks
                );
            }
        }
    }
}

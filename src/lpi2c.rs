use core::sync::atomic::spin_loop_hint;

use crate::dev;
use crate::generic::*;
use crate::hal::blocking::i2c;

// Teensy4 has only LPI2C1, 3, 4 broken out
pub use dev::{
    lpi2c1::{self as lpi2c, RegisterBlock},
    LPI2C1, LPI2C3, LPI2C4,
};

// TODO: nb-based API for master state checks/waits to enable timeouts

impl _Function for LPI2C1 {}
impl _Function for LPI2C3 {}
impl _Function for LPI2C4 {}

ccm_enable!(LPI2C1, ccgr2, cg3);
ccm_enable!(LPI2C3, ccgr2, cg5);
ccm_enable!(LPI2C4, ccgr6, cg12);

#[derive(Copy, Clone, Debug)]
pub enum Error {
    /// Bus busy (timeout)
    BusBusy,

    /// Master busy (timeout)
    MasterBusy,

    /// Will set when the SCL and/or SDA input is low for more than
    /// PINLOW cycles (Pin Low Timeout, MCFGR3\[PINLOW]), even when
    /// the LPI2C master is idle.
    PinLowTimeout,

    /// Detects an attempt to send or receive data without first
    /// generating a (repeated) START condition.
    FifoError,

    /// Set:
    /// - if the LPI2C master transmits a logic one and detects a
    ///   logic zero on the I2C bus
    /// - or if the LPI2C master detects a START or STOP condition
    ///   while the LPI2C master is transmitting data
    ArbitrationLost,

    /// Set if the LPI2C master detects a NACK when transmitting an
    /// address or data.
    NACKDetect,

    Timeout,

    /// We currently support only receiving less than 256 bytes.
    MoreThan256BytesToReceive,
}

// TODO: this should be deriveable
impl AsRef<str> for Error {
    fn as_ref(&self) -> &'static str {
        use Error::*;
        match self {
            BusBusy => "BusBusy",
            MasterBusy => "MasterBusy",
            PinLowTimeout => "PinLowTimeout",
            FifoError => "FifoError",
            ArbitrationLost => "ArbitrationLost",
            NACKDetect => "NACKDetect",
            Timeout => "Timeout",
            MoreThan256BytesToReceive => "MoreThan256BytesToReceive",
        }
    }
}

pub struct LPI2C<T>(T)
where
    T: Deref<Target = RegisterBlock>;

impl<T> From<T> for LPI2C<T>
where
    T: Deref<Target = RegisterBlock>,
{
    fn from(lpi2c: T) -> Self {
        Self(lpi2c)
    }
}

/// Iterations of wait() loop after which we give up
const WAIT_TIMEOUT: u32 = 10_000_000;

impl<T> LPI2C<T>
where
    T: Deref<Target = RegisterBlock>,
{
    pub fn checked_msr(&self) -> Result<lpi2c::msr::R, Error> {
        let r = self.0.msr.read();
        if r.pltf().bit_is_set() {
            return Err(Error::PinLowTimeout);
        }
        if r.fef().bit_is_set() {
            return Err(Error::FifoError);
        }
        if r.alf().bit_is_set() {
            return Err(Error::ArbitrationLost);
        }
        if r.ndf().bit_is_set() {
            return Err(Error::NACKDetect);
        }
        return Ok(r);
    }

    pub fn clear_flags(&mut self) {
        self.0.msr.write(|w| {
            w.dmf()
                .set_bit()
                .pltf()
                .set_bit()
                .fef()
                .set_bit()
                .alf()
                .set_bit()
                .ndf()
                .set_bit()
                .sdf()
                .set_bit()
                .sdf()
                .set_bit()
                .epf()
                .set_bit()
        });
    }

    pub fn clear_fifo(&mut self) {
        self.0.mcr.modify(|_r, w| w.rrf().set_bit().rtf().set_bit());
    }

    /// Blocks until condition returns true
    fn wait<F>(&self, condition: F) -> Result<(), Error>
    where
        F: Fn(lpi2c::msr::R) -> bool,
    {
        for _i in 0..WAIT_TIMEOUT {
            if condition(self.checked_msr()?) {
                return Ok(());
            }
        }
        return Err(Error::Timeout);
    }

    fn _write(&mut self, addr: u8, bytes: &[u8]) -> Result<(), Error> {
        // TODO: wait until master not busy?
        // TODO: check flags at entry?
        // TODO: clear EPF & SDF?

        // wait until there's place in tx queue
        self.wait(|msr| msr.tdf().bit_is_set())?;

        // START condition
        self.0
            .mtdr
            .write(|w| unsafe { w.data().bits(addr << 1) }.cmd().cmd_4());

        // data
        for byte in bytes {
            self.wait(|msr| msr.tdf().bit_is_set())?;
            self.0.mtdr.write(|w| unsafe { w.data().bits(*byte) });
        }

        // STOP condition
        self.wait(|msr| msr.tdf().bit_is_set())?;
        self.0.mtdr.write(|w| w.cmd().cmd_2());

        // wait until master not busy (is this necessary?)
        // self.wait(|msr| msr.mbf().is_mbf_0());

        // wait until packet end
        self.wait(|msr| msr.epf().bit_is_set())?;

        // clear EPF & SDF
        self.0.msr.write(|w| w.epf().set_bit().sdf().set_bit());

        Ok(())
    }

    fn _read(&mut self, addr: u8, buffer: &mut [u8]) -> Result<(), Error> {
        // TODO: wait until master not busy?
        // TODO: check flags at entry?

        if buffer.len() > 256 {
            return Err(Error::MoreThan256BytesToReceive);
        }

        if buffer.len() == 0 {
            return Ok(());
        }

        // wait until there's place in tx queue
        self.wait(|msr| msr.tdf().bit_is_set())?;

        // START condition
        self.0
            .mtdr
            .write(|w| unsafe { w.data().bits(addr << 1 | 1) }.cmd().cmd_4());

        // wait until there's place in tx queue
        self.wait(|msr| msr.tdf().bit_is_set())?;

        // Receive N+1 bytes
        self.0.mtdr.write(|w| {
            unsafe { w.data().bits((buffer.len() - 1) as u8) }
                .cmd()
                .cmd_1()
        });

        // Receive data
        for i in 0..buffer.len() {
            let mut j = 0;
            loop {
                self.checked_msr()?;
                let mrdr = self.0.mrdr.read();
                if !mrdr.rxempty().bit_is_set() {
                    buffer[i] = mrdr.data().bits();
                    break;
                }
                j += 1;
                if j > WAIT_TIMEOUT {
                    return Err(Error::Timeout);
                }
                spin_loop_hint();
            }
        }

        // STOP condition
        //self.wait(|msr| msr.rdf().bit_is_set())?;
        self.0.mtdr.write(|w| w.cmd().cmd_2());

        // wait until master not busy (is this necessary?)
        // self.wait(|msr| msr.mbf().is_mbf_0());

        // wait until packet end
        self.wait(|msr| msr.epf().bit_is_set())?;

        // clear EPF & SDF
        self.0.msr.write(|w| w.epf().set_bit().sdf().set_bit());

        Ok(())
    }

    fn _write_read(&mut self, addr: u8, bytes: &[u8], buffer: &mut [u8]) -> Result<(), Error> {
        // TODO: wait until master not busy?
        // TODO: check flags at entry?
        // TODO: clear EPF & SDF?

        if buffer.len() > 256 {
            return Err(Error::MoreThan256BytesToReceive);
        }

        // wait until there's place in tx queue
        self.wait(|msr| msr.tdf().bit_is_set())?;

        // START condition
        self.0
            .mtdr
            .write(|w| unsafe { w.data().bits(addr << 1) }.cmd().cmd_4());

        // Send data
        for byte in bytes {
            self.wait(|msr| msr.tdf().bit_is_set())?;
            self.0.mtdr.write(|w| unsafe { w.data().bits(*byte) });
        }

        // Repeated START condition
        self.0
            .mtdr
            .write(|w| unsafe { w.data().bits(addr << 1 | 1) }.cmd().cmd_4());

        // wait until packet end
        self.wait(|msr| msr.epf().bit_is_set())?;

        // clear EPF
        self.0.msr.write(|w| w.epf().set_bit());

        if buffer.len() > 0 {
            // wait until there's place in tx queue
            self.wait(|msr| msr.tdf().bit_is_set())?;

            // Receive N+1 bytes
            self.0.mtdr.write(|w| {
                unsafe { w.data().bits((buffer.len() - 1) as u8) }
                    .cmd()
                    .cmd_1()
            });

            // Receive data
            for i in 0..buffer.len() {
                let mut j = 0;
                loop {
                    self.checked_msr()?;
                    let mrdr = self.0.mrdr.read();
                    if !mrdr.rxempty().bit_is_set() {
                        buffer[i] = mrdr.data().bits();
                        break;
                    }
                    j += 1;
                    if j > WAIT_TIMEOUT {
                        return Err(Error::Timeout);
                    }
                }
            }
        }

        // STOP condition
        self.wait(|msr| msr.tdf().bit_is_set())?;
        self.0.mtdr.write(|w| w.cmd().cmd_2());

        // wait until master not busy (is this necessary?)
        // self.wait(|msr| msr.mbf().is_mbf_0());

        // wait until packet end
        self.wait(|msr| msr.epf().bit_is_set())?;

        // clear EPF & SDF
        self.0.msr.write(|w| w.epf().set_bit().sdf().set_bit());

        Ok(())
    }

    fn with_clear_flags<F>(&mut self, mut cb: F) -> Result<(), Error>
    where
        F: FnMut(&mut Self) -> Result<(), Error>,
    {
        if let Err(err) = cb(self) {
            while self.0.msr.read().mbf().bit_is_set() {
                spin_loop_hint();
            }
            self.clear_fifo();
            self.clear_flags();
            Err(err)
        } else {
            Ok(())
        }
    }
}

impl<T> i2c::Write for LPI2C<T>
where
    T: Deref<Target = RegisterBlock>,
{
    type Error = Error;

    fn write(&mut self, addr: u8, bytes: &[u8]) -> Result<(), Self::Error> {
        self.with_clear_flags(|myself| myself._write(addr, bytes))
    }
}

impl<T> i2c::Read for LPI2C<T>
where
    T: Deref<Target = RegisterBlock>,
{
    type Error = Error;

    fn read(&mut self, addr: u8, buffer: &mut [u8]) -> Result<(), Self::Error> {
        self.with_clear_flags(|myself| myself._read(addr, buffer))
    }
}

impl<T> i2c::WriteRead for LPI2C<T>
where
    T: Deref<Target = RegisterBlock>,
{
    type Error = Error;

    fn write_read(&mut self, addr: u8, bytes: &[u8], buffer: &mut [u8]) -> Result<(), Self::Error> {
        self.with_clear_flags(|myself| myself._write_read(addr, bytes, buffer))
    }
}

pub trait CcmExtEnable<T> {
    fn enable(&mut self, periph: &T);
}

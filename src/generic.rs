pub(crate) use core::marker::PhantomData;
pub(crate) use core::ops::Deref;

/// Trait to use a configuration object, implemented for register writers
pub trait Apply<T> {
    // TODO: AsRef<T>? Somehow make it a borrow on T, not a consume, but
    // without risking pessimization of generated code

    /// Apply configuration to self
    fn apply(&mut self, cfg: T) -> &mut Self;
}

/// Implements From<$enum> for another enum $variant
///
/// ```
/// impl_into_variant!(
///     Polarity,
///     dev::flexio1::timctl::TRGPOL_A,
///     ActiveHigh => TRGPOL_0,
///     ActiveLow => TRGPOL_1,
/// );
/// ```
macro_rules! impl_into_variant {
    ($enum:ident, $variant:path, $($from:pat => $to:ident,)+) => {
        impl From<$enum> for $variant {
            #[inline(always)]
            fn from(val: $enum) -> Self {
                use $enum::*;
                match val {
                    $( $from => Self::$to, )+
                }
            }
        }
    }
}

/// Implements Apply trait that casts $enum as $field's variant.
///
/// Mostly used together with `impl_into_variant!`
///
/// For example,
/// ```
/// impl_apply_as_field_variant!(TimerDisable, timcfg, timdis);
/// ```
/// implements `Apply<TimerDisable>` for `timcfg::W` as
/// ```
/// self.timdis().variant(timer_disable.into())
/// ```
macro_rules! impl_apply_as_field_variant {
    ($enum:ident, $reg:ident, $field:ident) => {
        impl Apply<$enum> for $reg::W {
            #[inline(always)]
            fn apply(&mut self, val: $enum) -> &mut Self {
                self.$field().variant(val.into())
            }
        }
    };
}

// Marker trait for pin function
pub trait _Function {}

// Unknown function
impl _Function for () {}

// Implement ptr() as a trait to be able to use this in a generic. TODO: add this feature to svd2rust?

#[allow(missing_docs)]
#[doc(hidden)]
pub trait _RegisterBlock {
    type Target;

    unsafe fn regblock() -> &'static Self::Target;
}

macro_rules! register_block {
    ($periph:ident, $register_block_module:ident) => {
        impl _RegisterBlock for dev::$periph {
            type Target = dev::$register_block_module::RegisterBlock;

            #[inline(always)]
            unsafe fn regblock() -> &'static Self::Target {
                &*Self::ptr()
            }
        }
    };
}

pub use crate::ccm::CcmExtEnable;

macro_rules! ccm_enable {
    ($periph:path, $ccgr:ident, $cg:ident) => {
        impl CcmExtEnable<$periph> for crate::dev::CCM {
            #[inline(always)]
            fn enable(&mut self, _periph: &$periph) {
                self.$ccgr.modify(|_r, w| unsafe { w.$cg().bits(3) });
            }
        }
    };
}

pub use crate::dma::DmamuxSource;

macro_rules! dmamux_source {
    ($periph:path, $source:expr) => {
        impl DmamuxSource for $periph {
            #[inline(always)]
            fn dmamux_source(&self) -> u8 {
                $source
            }
        }
    };
}

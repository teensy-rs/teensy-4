use core::marker::PhantomData;
use typenum;

use crate::dev::gpio1::RegisterBlock;
use crate::generic::*;
use crate::hal::digital::v2 as hal;

// Only these four GPIOs seem used/usable
register_block!(GPIO6, gpio1);
register_block!(GPIO7, gpio1);
register_block!(GPIO8, gpio1);
register_block!(GPIO9, gpio1);

// TODO: interrupts / interrupt mask
// TODO: pad status?

pub trait State {}
pub struct Input;
pub struct Output;
impl State for Input {}
impl State for Output {}
impl State for () {}

pub struct GPIOPin<TMask, TGPIO, TState>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
    TState: State,
{
    _gpio: PhantomData<TGPIO>,
    _mask: PhantomData<TMask>,
    _state: PhantomData<TState>,
}

impl<TMask, TGPIO, TState> _Function for GPIOPin<TMask, TGPIO, TState>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
    TState: State,
{
}

impl<TMask, TGPIO, TState> GPIOPin<TMask, TGPIO, TState>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
    TState: State,
{
    #[inline(always)]
    pub fn mask() -> u32 {
        // TODO: const fn?
        TMask::to_u32()
    }

    pub unsafe fn fiat() -> Self {
        Self {
            _gpio: PhantomData,
            _mask: PhantomData,
            _state: PhantomData,
        }
    }

    fn regblock() -> &'static RegisterBlock {
        unsafe { TGPIO::regblock() }
    }

    fn is_set(&self) -> bool {
        (Self::regblock().dr.read().bits() & Self::mask()) != 0
    }

    pub fn into_input(self) -> GPIOPin<TMask, TGPIO, Input> {
        Self::regblock()
            .gdir
            .modify(|r, w| unsafe { w.gdir().bits(r.gdir().bits() & !Self::mask()) });
        unsafe { GPIOPin::<TMask, TGPIO, Input>::fiat() }
    }

    pub fn into_output(self) -> GPIOPin<TMask, TGPIO, Output> {
        Self::regblock()
            .gdir
            .modify(|r, w| unsafe { w.gdir().bits(r.gdir().bits() | Self::mask()) });
        unsafe { GPIOPin::<TMask, TGPIO, Output>::fiat() }
    }
}

impl<TMask, TGPIO> hal::OutputPin for GPIOPin<TMask, TGPIO, Output>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
{
    type Error = !;

    fn set_low(&mut self) -> Result<(), Self::Error> {
        Self::regblock()
            .dr_clear
            .write_with_zero(|w| unsafe { w.bits(Self::mask()) });
        Ok(())
    }

    fn set_high(&mut self) -> Result<(), Self::Error> {
        Self::regblock()
            .dr_set
            .write_with_zero(|w| unsafe { w.bits(Self::mask()) });
        Ok(())
    }
}

impl<TMask, TGPIO> hal::StatefulOutputPin for GPIOPin<TMask, TGPIO, Output>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
{
    fn is_set_low(&self) -> Result<bool, Self::Error> {
        Ok(!self.is_set())
    }

    fn is_set_high(&self) -> Result<bool, Self::Error> {
        Ok(self.is_set())
    }
}

impl<TMask, TGPIO> hal::ToggleableOutputPin for GPIOPin<TMask, TGPIO, Output>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
{
    type Error = !;

    fn toggle(&mut self) -> Result<(), Self::Error> {
        Self::regblock()
            .dr_toggle
            .write_with_zero(|w| unsafe { w.bits(Self::mask()) });
        Ok(())
    }
}

impl<TMask, TGPIO> hal::InputPin for GPIOPin<TMask, TGPIO, Input>
where
    TMask: typenum::Unsigned + typenum::PowerOfTwo,
    TGPIO: _RegisterBlock<Target = RegisterBlock>,
{
    type Error = !;

    fn is_low(&self) -> Result<bool, Self::Error> {
        Ok(!self.is_set())
    }

    fn is_high(&self) -> Result<bool, Self::Error> {
        Ok(self.is_set())
    }
}

// Teensy GPIO pins
macro_rules! gpio_pin {
    ($pin:ident, $gpio:ident, $bit:ident) => {
        pub type $pin<TState> =
            GPIOPin<typenum::Shleft<typenum::U1, typenum::$bit>, dev::$gpio, TState>;
    };
}

gpio_pin!(Pin0, GPIO6, U3);
gpio_pin!(Pin1, GPIO6, U2);
gpio_pin!(Pin2, GPIO9, U4);
gpio_pin!(Pin3, GPIO9, U5);
gpio_pin!(Pin4, GPIO9, U6);
gpio_pin!(Pin5, GPIO9, U8);
gpio_pin!(Pin6, GPIO7, U10);
gpio_pin!(Pin7, GPIO7, U17);
gpio_pin!(Pin8, GPIO7, U16);
gpio_pin!(Pin9, GPIO7, U11);
gpio_pin!(Pin10, GPIO7, U0);
gpio_pin!(Pin11, GPIO7, U2);
gpio_pin!(Pin12, GPIO7, U1);
gpio_pin!(Pin13, GPIO7, U3);
gpio_pin!(Pin14, GPIO6, U18);
gpio_pin!(Pin15, GPIO6, U19);
gpio_pin!(Pin16, GPIO6, U23);
gpio_pin!(Pin17, GPIO6, U22);
gpio_pin!(Pin18, GPIO6, U17);
gpio_pin!(Pin19, GPIO6, U16);
gpio_pin!(Pin20, GPIO6, U26);
gpio_pin!(Pin21, GPIO6, U27);
gpio_pin!(Pin22, GPIO6, U24);
gpio_pin!(Pin23, GPIO6, U25);
gpio_pin!(Pin24, GPIO6, U12);
gpio_pin!(Pin25, GPIO6, U13);
gpio_pin!(Pin26, GPIO6, U30);
gpio_pin!(Pin27, GPIO6, U31);
gpio_pin!(Pin28, GPIO8, U18);
gpio_pin!(Pin29, GPIO9, U31);
gpio_pin!(Pin30, GPIO8, U23);
gpio_pin!(Pin31, GPIO8, U22);
gpio_pin!(Pin32, GPIO7, U12);
gpio_pin!(Pin33, GPIO9, U7);
gpio_pin!(Pin34, GPIO8, U15);
gpio_pin!(Pin35, GPIO8, U14);
gpio_pin!(Pin36, GPIO8, U13);
gpio_pin!(Pin37, GPIO8, U12);
gpio_pin!(Pin38, GPIO8, U17);
gpio_pin!(Pin39, GPIO8, U16);

pub type PinLED = Pin13<Output>;

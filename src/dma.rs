use core::marker::PhantomData;

use typenum::{
    Unsigned, U0, U1, U10, U11, U12, U13, U14, U15, U16, U17, U18, U19, U2, U20, U21, U22, U23,
    U24, U25, U26, U27, U28, U29, U3, U30, U31, U4, U5, U6, U7, U8, U9,
};

use crate::dev;
use crate::generic::*;

pub trait DmamuxSource {
    fn dmamux_source(&self) -> u8;
}

pub trait DmamuxChcfgSourceWExt<'a, T> {
    fn select(self, source: &T) -> &'a mut dev::dmamux::chcfg::W;
}

impl<'a, T> DmamuxChcfgSourceWExt<'a, T> for dev::dmamux::chcfg::SOURCE_W<'a>
where
    T: DmamuxSource,
{
    #[inline(always)]
    fn select(self, source: &T) -> &'a mut dev::dmamux::chcfg::W {
        unsafe { self.bits(source.dmamux_source()) }
    }
}

ccm_enable!(dev::DMA0, ccgr5, cg3);

pub struct DmaChannel<Tn>
where
    Tn: Unsigned,
{
    _num: PhantomData<Tn>,
}

macro_rules! check_mask {
    ($reg:ident) => {
        #[inline(always)]
        pub fn $reg(&self) -> bool {
            self.dma().$reg.read().bits() & self.mask() != 0
        }
    }
}

macro_rules! set_bit {
    ($reg:ident) => {
        #[inline(always)]
        pub fn $reg(&mut self) {
            self.dma()
                .$reg
                .write(|w| unsafe { w.$reg().bits(self.num() as u8) });
        }
    }
}

impl<Tn> DmaChannel<Tn>
where
    Tn: Unsigned,
{
    pub unsafe fn fiat() -> Self {
        Self { _num: PhantomData }
    }

    #[inline(always)]
    fn num(&self) -> usize {
        Tn::to_usize()
    }

    #[inline(always)]
    fn mask(&self) -> u32 {
        1u32 << self.num() as u32
    }

    #[inline(always)]
    pub fn chcfg(&self) -> &dev::dmamux::CHCFG {
        &unsafe { &*dev::DMAMUX::ptr() }.chcfg[self.num()]
    }

    #[inline(always)]
    fn dma(&self) -> &dev::dma0::RegisterBlock {
        unsafe { &*dev::DMA0::ptr() }
    }

    #[inline(always)]
    pub fn tcd(&self) -> &dev::dma0::TCD {
        &self.dma().tcd[self.num()]
    }

    check_mask!(err);
    set_bit!(cerr);

    check_mask!(erq);
    set_bit!(serq);
    set_bit!(cerq);

    check_mask!(eei);
    set_bit!(seei);
    set_bit!(ceei);

    set_bit!(cdne);
    set_bit!(ssrt);

    check_mask!(int);
    set_bit!(cint);

    check_mask!(hrs);

    check_mask!(ears);
    // TODO: set/clear EARS bit (no nice shortcut registers)
}

pub type DmaChannels = (
    DmaChannel<U0>,
    DmaChannel<U1>,
    DmaChannel<U2>,
    DmaChannel<U3>,
    DmaChannel<U4>,
    DmaChannel<U5>,
    DmaChannel<U6>,
    DmaChannel<U7>,
    DmaChannel<U8>,
    DmaChannel<U9>,
    DmaChannel<U10>,
    DmaChannel<U11>,
    DmaChannel<U12>,
    DmaChannel<U13>,
    DmaChannel<U14>,
    DmaChannel<U15>,
    DmaChannel<U16>,
    DmaChannel<U17>,
    DmaChannel<U18>,
    DmaChannel<U19>,
    DmaChannel<U20>,
    DmaChannel<U21>,
    DmaChannel<U22>,
    DmaChannel<U23>,
    DmaChannel<U24>,
    DmaChannel<U25>,
    DmaChannel<U26>,
    DmaChannel<U27>,
    DmaChannel<U28>,
    DmaChannel<U29>,
    DmaChannel<U30>,
    DmaChannel<U31>,
);

pub struct Dma {
    _dmamux: dev::DMAMUX,
    dma0: dev::DMA0,
}

impl Dma {
    #[inline(always)]
    pub fn es(&self) -> &dev::dma0::ES {
        &self.dma0.es
    }
}

impl CcmExtEnable<Dma> for crate::dev::CCM {
    #[inline(always)]
    fn enable(&mut self, dma: &Dma) {
        self.enable(&dma.dma0);
    }
}

pub trait DmaExt {
    fn split(self, _dmamux: dev::DMAMUX) -> (Dma, DmaChannels);
}

impl DmaExt for dev::DMA0 {
    fn split(self, dmamux: dev::DMAMUX) -> (Dma, DmaChannels) {
        (
            Dma {
                dma0: self,
                _dmamux: dmamux,
            },
            (
                unsafe { DmaChannel::<U0>::fiat() },
                unsafe { DmaChannel::<U1>::fiat() },
                unsafe { DmaChannel::<U2>::fiat() },
                unsafe { DmaChannel::<U3>::fiat() },
                unsafe { DmaChannel::<U4>::fiat() },
                unsafe { DmaChannel::<U5>::fiat() },
                unsafe { DmaChannel::<U6>::fiat() },
                unsafe { DmaChannel::<U7>::fiat() },
                unsafe { DmaChannel::<U8>::fiat() },
                unsafe { DmaChannel::<U9>::fiat() },
                unsafe { DmaChannel::<U10>::fiat() },
                unsafe { DmaChannel::<U11>::fiat() },
                unsafe { DmaChannel::<U12>::fiat() },
                unsafe { DmaChannel::<U13>::fiat() },
                unsafe { DmaChannel::<U14>::fiat() },
                unsafe { DmaChannel::<U15>::fiat() },
                unsafe { DmaChannel::<U16>::fiat() },
                unsafe { DmaChannel::<U17>::fiat() },
                unsafe { DmaChannel::<U18>::fiat() },
                unsafe { DmaChannel::<U19>::fiat() },
                unsafe { DmaChannel::<U20>::fiat() },
                unsafe { DmaChannel::<U21>::fiat() },
                unsafe { DmaChannel::<U22>::fiat() },
                unsafe { DmaChannel::<U23>::fiat() },
                unsafe { DmaChannel::<U24>::fiat() },
                unsafe { DmaChannel::<U25>::fiat() },
                unsafe { DmaChannel::<U26>::fiat() },
                unsafe { DmaChannel::<U27>::fiat() },
                unsafe { DmaChannel::<U28>::fiat() },
                unsafe { DmaChannel::<U29>::fiat() },
                unsafe { DmaChannel::<U30>::fiat() },
                unsafe { DmaChannel::<U31>::fiat() },
            ),
        )
    }
}

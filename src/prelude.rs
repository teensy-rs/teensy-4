pub use crate::hal::prelude::*;

// unexported HAL
pub use crate::hal::digital::v2::InputPin as _embedded_hal_digital_v2_InputPin;
pub use crate::hal::digital::v2::OutputPin as _embedded_hal_digital_v2_OutputPin;
pub use crate::hal::digital::v2::StatefulOutputPin as _embedded_hal_digital_v2_StatefulOutputPin;
pub use crate::hal::digital::v2::ToggleableOutputPin as _embedded_hal_digital_v2_ToggleableOutputPin;

pub use crate::ccm::CcmExtEnable as _teensy4_ccm_CcmExtEnable;
pub use crate::dma::DmaExt as _teensy4_dma_DmaExt;
pub use crate::dma::DmamuxChcfgSourceWExt as _teensy4_dma_DmamuxChcfgSourceWExt;
pub use crate::dma::DmamuxSource as _teensy4_dma_DmamuxSource;
pub use crate::enc::EncExt as _teensy4_enc_EncExt;
pub use crate::flexio::FlexioExt as _teensy4_flexio_FlexioExt;
pub use crate::generic::Apply as _teensy4_generic_Apply;
pub use crate::snvs::SnvsExt as _teensy4_snvs_SnvsExt;
pub use crate::xbar::Xbar1Ext as _teensy4_xbar_Xbar1Ext;

use crate::dev;
use crate::platform::{asm, register};
use core::cmp::{max, min};
use core::ptr;

use cortex_m_rt::pre_init;
pub use cortex_m_rt::{self as platform, entry, exception};

macro_rules! unsafe_periph {
    ($name:ident) => {
        unsafe { &*(dev::$name::ptr()) }
    };
}

// FlexSPI configuration block. Maybe it should be in the linker script instead?
#[doc(hidden)]
#[link_section = ".flashconfig"]
#[no_mangle]
pub static __FlexSPI_NOR_Config: [u32; 128] = [
    // 448 byte common FlexSPI configuration block, 8.6.3.1 page 223 (RT1060 rev 0)
    // MCU_Flashloader_Reference_Manual.pdf, 8.2.1, Table 8-2, page 72-75
    0x42464346, // Tag				0x00
    0x56010000, // Version
    0,          // reserved
    0x00020101, // columnAdressWidth,dataSetupTime,dataHoldTime,readSampleClkSrc
    0x00000000, // waitTimeCfgCommands,-,deviceModeCfgEnable
    0,          // deviceModeSeq
    0,          // deviceModeArg
    0x00000000, // -,-,-,configCmdEnable
    0,          // configCmdSeqs		0x20
    0, 0, 0, 0, // cfgCmdArgs			0x30
    0, 0, 0, 0x00000000, // controllerMiscOption		0x40
    0x00030401, // lutCustomSeqEnable,serialClkFreq,sflashPadType,deviceType
    0,          // reserved
    0,          // reserved
    0x00200000, // sflashA1Size			0x50
    0,          // sflashA2Size
    0,          // sflashB1Size
    0,          // sflashB2Size
    0,          // csPadSettingOverride		0x60
    0,          // sclkPadSettingOverride
    0,          // dataPadSettingOverride
    0,          // dqsPadSettingOverride
    0,          // timeoutInMs			0x70
    0,          // commandInterval
    0,          // dataValidTime
    0x00000000, // busyBitPolarity,busyOffset
    0x0A1804EB, // lookupTable[0]		0x80
    0x26043206, // lookupTable[1]
    0,          // lookupTable[2]
    0,          // lookupTable[3]
    0x24040405, // lookupTable[4]		0x90
    0,          // lookupTable[5]
    0,          // lookupTable[6]
    0,          // lookupTable[7]
    0,          // lookupTable[8]		0xA0
    0,          // lookupTable[9]
    0,          // lookupTable[10]
    0,          // lookupTable[11]
    0x00000406, // lookupTable[12]		0xB0
    0,          // lookupTable[13]
    0,          // lookupTable[14]
    0,          // lookupTable[15]
    0,          // lookupTable[16]		0xC0
    0,          // lookupTable[17]
    0,          // lookupTable[18]
    0,          // lookupTable[19]
    0x08180420, // lookupTable[20]		0xD0
    0,          // lookupTable[21]
    0,          // lookupTable[22]
    0,          // lookupTable[23]
    0,          // lookupTable[24]		0xE0
    0,          // lookupTable[25]
    0,          // lookupTable[26]
    0,          // lookupTable[27]
    0,          // lookupTable[28]		0xF0
    0,          // lookupTable[29]
    0,          // lookupTable[30]
    0,          // lookupTable[31]
    0x081804D8, // lookupTable[32]		0x100
    0,          // lookupTable[33]
    0,          // lookupTable[34]
    0,          // lookupTable[35]
    0x08180402, // lookupTable[36]		0x110
    0x00002004, // lookupTable[37]
    0,          // lookupTable[38]
    0,          // lookupTable[39]
    0,          // lookupTable[40]		0x120
    0,          // lookupTable[41]
    0,          // lookupTable[42]
    0,          // lookupTable[43]
    0x00000460, // lookupTable[44]		0x130
    0,          // lookupTable[45]
    0,          // lookupTable[46]
    0,          // lookupTable[47]
    0,          // lookupTable[48]		0x140
    0,          // lookupTable[49]
    0,          // lookupTable[50]
    0,          // lookupTable[51]
    0,          // lookupTable[52]		0x150
    0,          // lookupTable[53]
    0,          // lookupTable[54]
    0,          // lookupTable[55]
    0,          // lookupTable[56]		0x160
    0,          // lookupTable[57]
    0,          // lookupTable[58]
    0,          // lookupTable[59]
    0,          // lookupTable[60]		0x170
    0,          // lookupTable[61]
    0,          // lookupTable[62]
    0,          // lookupTable[63]
    0,          // LUT 0: Read			0x180
    0,          // LUT 1: ReadStatus
    0,          // LUT 3: WriteEnable
    0,          // LUT 5: EraseSector
    0,          // LUT 9: PageProgram		0x190
    0,          // LUT 11: ChipErase
    0,          // LUT 15: Dummy
    0,          // LUT unused?
    0,          // LUT unused?			0x1A0
    0,          // LUT unused?
    0,          // LUT unused?
    0,          // LUT unused?
    0,          // reserved			0x1B0
    0,          // reserved
    0,          // reserved
    0,          // reserved
    // 64 byte Serial NOR configuration block, 8.6.3.2, page 346
    256,        // pageSize			0x1C0
    4096,       // sectorSize
    1,          // ipCmdSerialClkFreq
    0,          // reserved
    0x00010000, // block size			0x1D0
    0,          // reserved
    0,          // reserved
    0,          // reserved
    0,          // reserved			0x1E0
    0,          // reserved
    0,          // reserved
    0,          // reserved
    0,          // reserved			0x1F0
    0,          // reserved
    0,          // reserved
    0,          // reserved
];

// mimxrt1062 crate defines 158-long interrupt vector; teensy4 core uses 160
const NVIC_NUM_INTERRUPTS: usize = 160;
const VECTOR_TABLE_LENGTH: usize = NVIC_NUM_INTERRUPTS + 16;

// Place to copy the vector table to RAM. TODO: use the Vector type?
// Manage changing handlers?
// FIXME: this must be 1024-aligned; currently it relies on it being first/only thing in .uninit section
#[doc(hidden)]
#[no_mangle]
#[link_section = ".uninit.vtor"]
pub static mut __VTOR: [u32; VECTOR_TABLE_LENGTH] = [0; VECTOR_TABLE_LENGTH]; // actually not zero, but we write it early

#[pre_init]
unsafe fn early_setup() {
    // Configure FlexRAM and initialize stack.
    extern "C" {
        static _flexram_bank_config: u8; // u8 because it's odd value, *u32 would be unaligned
        static _stack_start: u32;
    }
    let _flexram_bank_config_as_p: *const u8 = &_flexram_bank_config;
    let flexram_bank_config: u32 = _flexram_bank_config_as_p as u32;
    let stack_start_p: *const u32 = &_stack_start;
    let iomuxc_gpr_gpr17: *mut u32 = 0x400AC044 as *mut u32;
    let iomuxc_gpr_gpr16: *mut u32 = 0x400AC040 as *mut u32;
    let iomuxc_gpr_gpr14: *mut u32 = 0x400AC038 as *mut u32;
    ptr::write_volatile(iomuxc_gpr_gpr17, flexram_bank_config);
    ptr::write_volatile(iomuxc_gpr_gpr16, 0x00000007);
    ptr::write_volatile(iomuxc_gpr_gpr14, 0x00AA0000);

    // TODO: make sure stack hasn't moved, copy existing stack &
    // ajdust if necessary (in debug build, this function might not be
    // inlined)
    register::msp::write(stack_start_p as u32);
    register::psp::write(stack_start_p as u32);

    // Copy .text.itcm section from flash to ITCM.
    extern "C" {
        static mut __stext: u32;
        static mut __etext: u32;
        static __sitext: u32;
    }

    let mut stext_p: *mut u32 = &mut __stext;
    let etext_p: *mut u32 = &mut __etext;
    let mut sitext_p: *const u32 = &__sitext;
    while stext_p <= etext_p {
        ptr::write(stext_p, ptr::read(sitext_p));
        stext_p = stext_p.offset(1);
        sitext_p = sitext_p.offset(1);
    }

    // Copy the vector table from flash to RAM
    extern "C" {
        static __VectorTable: u32;
    }
    let mut flash_vt_p: *const u32 = &__VectorTable;
    let mut ram_vt_p: *mut u32 = &mut __VTOR[0];
    let e_ram_vt_p: *mut u32 = ram_vt_p.offset(VECTOR_TABLE_LENGTH as isize);
    while ram_vt_p < e_ram_vt_p {
        ptr::write(ram_vt_p, ptr::read(flash_vt_p));
        flash_vt_p = flash_vt_p.offset(1);
        ram_vt_p = ram_vt_p.offset(1);
    }

    // set all interrupts to middle priority
    let nvic_priority_p: *mut u8 = 0xE000_E400 as *mut u8;
    for i in 0..NVIC_NUM_INTERRUPTS as isize {
        ptr::write(nvic_priority_p.offset(i), 128);
    }

    // Set vector table (write SCB_VTOR)
    const SCB_VTOR: *mut u32 = 0xE000_ED08 as *mut u32;
    ptr::write_volatile(SCB_VTOR, &__VTOR[0] as *const u32 as u32);
}

/// General initialization. Call at the beginning of main() or proceed
/// at your own risk. Might be too specific, pieces might be moved to
/// HAL or safer periph-specific code in the future.
/// Returns actual frequency of main clock.
/// Inspired by https://github.com/PaulStoffregen/cores/blob/master/teensy4/startup.c
pub fn init_teensy() -> u32 {
    reset_pfd();
    configure_clocks();
    configure_gpio(); // This might be more of HAL trait's thing?
    unconfigure_pit();

    // must enable PRINT_DEBUG_STUFF in debug/print.h
    // printf_debug_init();
    // printf("\n***********IMXRT Startup**********\n");
    // printf("test %d %d %d\n", 1, -1234567, 3);

    configure_cache();
    configure_systick(); // This should also be HAL or such (but systick is used later in the NFY part to wait)
    usb_pll_start();

    reset_pfd(); // upstream: TODO: is this really needed?

    let freq = set_arm_clock(600_000_000);
    //set_arm_clock(984000000); Ludicrous Speed

    // initialize RTC
    // if (!(SNVS_LPCR & SNVS_LPCR_SRTC_ENV)) {
    //         // if SRTC isn't running, start it with default Jan 1, 2019
    //         SNVS_LPSRTCLR = 1546300800u << 15;
    //         SNVS_LPSRTCMR = 1546300800u >> 17;
    //         SNVS_LPCR |= SNVS_LPCR_SRTC_ENV;
    // }
    // SNVS_HPCR |= SNVS_HPCR_RTC_EN | SNVS_HPCR_HP_TS;

    // while (millis() < 20) ; // wait at least 20ms before starting USB
    // usb_init();
    // analog_init();
    // pwm_init();
    // tempmon_init();

    // while (millis() < 300) ; // wait at least 300ms before calling user code
    // //printf("before C++ constructors\n");
    // __libc_init_array();
    // //printf("after C++ constructors\n");
    // //printf("before setup\n");
    // setup();
    // //printf("after setup\n");
    // while (1) {
    //         //printf("loop\n");
    //         loop();
    //         yield();
    // }

    return freq;
}

fn reset_pfd() {
    let ccm_analog = unsafe { &*(dev::CCM_ANALOG::ptr()) };

    //Reset PLL2 PFDs, set default frequencies:
    //CCM_ANALOG_PFD_528_SET = (1 << 31) | (1 << 23) | (1 << 15) | (1 << 7);
    ccm_analog.pfd_528_set.write_with_zero(|w| {
        w.pfd0_clkgate()
            .set_bit()
            .pfd1_clkgate()
            .set_bit()
            .pfd2_clkgate()
            .set_bit()
            .pfd3_clkgate()
            .set_bit()
    });
    //CCM_ANALOG_PFD_528 = 0x2018101B; // PFD0:352, PFD1:594, PFD2:396, PFD3:297 MHz
    ccm_analog.pfd_528.write(|w| unsafe {
        w.pfd0_frac()
            .bits(0x1B)
            .pfd1_frac()
            .bits(0x10)
            .pfd2_frac()
            .bits(0x18)
            .pfd3_frac()
            .bits(0x20)
    });

    //PLL3:
    //CCM_ANALOG_PFD_480_SET = (1 << 31) | (1 << 23) | (1 << 15) | (1 << 7);
    ccm_analog.pfd_480_set.write_with_zero(|w| {
        w.pfd0_clkgate()
            .set_bit()
            .pfd1_clkgate()
            .set_bit()
            .pfd2_clkgate()
            .set_bit()
            .pfd3_clkgate()
            .set_bit()
    });
    //CCM_ANALOG_PFD_480 = 0x13110D0C; // PFD0:720, PFD1:664, PFD2:508, PFD3:454 MHz
    ccm_analog.pfd_528.write(|w| unsafe {
        w.pfd0_frac()
            .bits(0x0C)
            .pfd1_frac()
            .bits(0x0D)
            .pfd2_frac()
            .bits(0x11)
            .pfd3_frac()
            .bits(0x13)
    });
}

fn configure_clocks() {
    let ccm = unsafe_periph!(CCM);

    // TODO: make sure all affected peripherals are turned off!
    // PIT & GPT timers to run from 24 MHz clock (independent of CPU speed)
    // CCM_CSCMR1 = (CCM_CSCMR1 & ~CCM_CSCMR1_PERCLK_PODF(0x3F)) | CCM_CSCMR1_PERCLK_CLK_SEL;
    ccm.cscmr1
        .modify(|_r, w| w.perclk_podf().divide_1().perclk_clk_sel().set_bit());

    // UARTs run from 24 MHz clock (works if PLL3 off or bypassed)
    // CCM_CSCDR1 = (CCM_CSCDR1 & ~CCM_CSCDR1_UART_CLK_PODF(0x3F)) | CCM_CSCDR1_UART_CLK_SEL;
    ccm.cscdr1
        .modify(|_r, w| w.uart_clk_podf().divide_1().uart_clk_sel().set_bit());
}

fn configure_gpio() {
    let iomuxc_gpr = unsafe_periph!(IOMUXC_GPR);

    // // Use fast GPIO6, GPIO7, GPIO8, GPIO9
    iomuxc_gpr
        .gpr26
        .write(|w| unsafe { w.gpio_mux1_gpio_sel().bits(0xffff_ffff) });
    iomuxc_gpr
        .gpr27
        .write(|w| unsafe { w.gpio_mux2_gpio_sel().bits(0xffff_ffff) });
    iomuxc_gpr
        .gpr28
        .write(|w| unsafe { w.gpio_mux3_gpio_sel().bits(0xffff_ffff) });
    iomuxc_gpr
        .gpr29
        .write(|w| unsafe { w.gpio_mux4_gpio_sel().bits(0xffff_ffff) });
}

fn unconfigure_pit() {
    let ccm = unsafe_periph!(CCM);
    let pit = unsafe_periph!(PIT);

    // Undo PIT timer usage by ROM startup
    // CCM_CCGR1 |= CCM_CCGR1_PIT(CCM_CCGR_ON);
    ccm.ccgr1.modify(|_r, w| unsafe { w.cg6().bits(3) });

    // PIT_MCR = 0;
    pit.mcr.write_with_zero(|w| w);

    // PIT_TCTRL0 = 0;
    // PIT_TCTRL1 = 0;
    // PIT_TCTRL2 = 0;
    // PIT_TCTRL3 = 0;
    for tmr in &pit.timer {
        tmr.tctrl.write_with_zero(|w| w);
    }
}

// This is ugly, and implements only some bits.
// TODO: cleanup, generalize
const fn mpu_rbar_val(addr: u32, region: u8) -> u32 {
    // addr is aligned (lower 5 bits are 0)
    // region is 4 bits (0-15)
    addr | region as u32 | 1 << 4
}
const MPU_RASR_RW: u32 = 3 << 24;
const MPU_RASR_RO: u32 = 7 << 24;
const MPU_RASR_NOEXEC: u32 = 1 << 28;
const MPU_RASR_MEM_NOCACHE: u32 = 1 << 19;
const MPU_RASR_DEV_NOCACHE: u32 = 2 << 19;
const MPU_RASR_MEM_CACHE_WT: u32 = 1 << 17;
const MPU_RASR_MEM_CACHE_WBWA: u32 = 1 << 19 | 1 << 17 | 1 << 16;
const MPU_RASR_SIZE_128K: u32 = (16 << 1) | 1;
const MPU_RASR_SIZE_512K: u32 = (18 << 1) | 1;
const MPU_RASR_SIZE_1M: u32 = (19 << 1) | 1;
const MPU_RASR_SIZE_16M: u32 = (23 << 1) | 1;
const MPU_RASR_SIZE_64M: u32 = (25 << 1) | 1;

fn configure_cache() {
    let mpu = unsafe_periph!(MPU);
    let scb = unsafe_periph!(SCB);
    let cbp = unsafe_periph!(CBP);

    // //printf("MPU_TYPE = %08lX\n", SCB_MPU_TYPE);
    // //printf("CCR = %08lX\n", SCB_CCR);

    // // TODO: check if caches already active - skip?

    // SCB_MPU_CTRL = 0; // turn off MPU
    unsafe { mpu.ctrl.write(0) };

    // SCB_MPU_RBAR = 0x00000000 | REGION(0); // ITCM
    // SCB_MPU_RASR = MEM_NOCACHE | READWRITE | SIZE_512K;
    unsafe {
        mpu.rbar.write(mpu_rbar_val(0x00000000, 0));
        mpu.rasr
            .write(MPU_RASR_MEM_NOCACHE | MPU_RASR_RW | MPU_RASR_SIZE_512K);
    };

    // SCB_MPU_RBAR = 0x00200000 | REGION(1); // Boot ROM
    // SCB_MPU_RASR = MEM_CACHE_WT | READONLY | SIZE_128K;
    unsafe {
        mpu.rbar.write(mpu_rbar_val(0x00200000, 1));
        mpu.rasr
            .write(MPU_RASR_MEM_CACHE_WT | MPU_RASR_RO | MPU_RASR_SIZE_128K);
    }

    // SCB_MPU_RBAR = 0x20000000 | REGION(2); // DTCM
    // SCB_MPU_RASR = MEM_NOCACHE | READWRITE | NOEXEC | SIZE_512K;
    unsafe {
        mpu.rbar.write(mpu_rbar_val(0x20000000, 2));
        mpu.rasr
            .write(MPU_RASR_MEM_NOCACHE | MPU_RASR_RW | MPU_RASR_NOEXEC | MPU_RASR_SIZE_512K);
    };

    // SCB_MPU_RBAR = 0x20200000 | REGION(3); // RAM (AXI bus)
    // SCB_MPU_RASR = MEM_CACHE_WBWA | READWRITE | NOEXEC | SIZE_1M;
    unsafe {
        mpu.rbar.write(mpu_rbar_val(0x20200000, 3));
        mpu.rasr
            .write(MPU_RASR_MEM_CACHE_WBWA | MPU_RASR_RW | MPU_RASR_NOEXEC | MPU_RASR_SIZE_1M);
    };

    // SCB_MPU_RBAR = 0x40000000 | REGION(4); // Peripherals
    // SCB_MPU_RASR = DEV_NOCACHE | READWRITE | NOEXEC | SIZE_64M;
    unsafe {
        mpu.rbar.write(mpu_rbar_val(0x40000000, 4));
        mpu.rasr
            .write(MPU_RASR_DEV_NOCACHE | MPU_RASR_RW | MPU_RASR_NOEXEC | MPU_RASR_SIZE_64M);
    };

    // SCB_MPU_RBAR = 0x60000000 | REGION(5); // QSPI Flash
    // SCB_MPU_RASR = MEM_CACHE_WBWA | READONLY | SIZE_16M;
    unsafe {
        mpu.rbar.write(mpu_rbar_val(0x60000000, 5));
        mpu.rasr
            .write(MPU_RASR_MEM_CACHE_WBWA | MPU_RASR_RO | MPU_RASR_SIZE_16M);
    };

    // // TODO: 32 byte sub-region at 0x00000000 with NOACCESS, to trap NULL pointer deref
    // // TODO: protect access to power supply config
    // // TODO: 32 byte sub-region at end of .bss section with NOACCESS, to trap stack overflow

    // SCB_MPU_CTRL = SCB_MPU_CTRL_ENABLE;
    unsafe { mpu.ctrl.write(1) };

    // // cache enable, ARM DDI0403E, pg 628
    // asm("dsb");
    // asm("isb");
    // SCB_CACHE_ICIALLU = 0;
    asm::dsb();
    asm::isb();
    unsafe { cbp.iciallu.write(0) };

    // asm("dsb");
    // asm("isb");
    // SCB_CCR |= (SCB_CCR_IC | SCB_CCR_DC);
    asm::dsb();
    asm::isb();
    unsafe { scb.ccr.modify(|r| r | (1 << 17) | (1 << 16)) };

    // TODO: This should also work?
    // scb.enable_icache();
    // scb.enable_dcache(); // <- this requires a CPUID, need to investiga
}

// ARM SysTick is used for most Ardiuno timing functions, delay(), millis(),
// micros().  SysTick can run from either the ARM core clock, or from an
// "external" clock.  NXP documents it as "24 MHz XTALOSC can be the external
// clock source of SYSTICK" (RT1052 ref manual, rev 1, page 411).  However,
// NXP actually hid an undocumented divide-by-240 circuit in the hardware, so
// the external clock is really 100 kHz.  We use this clock rather than the
// ARM clock, to allow SysTick to maintain correct timing even when we change
// the ARM clock to run at different speeds.
pub const SYSTICK_EXT_FREQ: u32 = 100_000;

// extern volatile uint32_t systick_cycle_count;
fn configure_systick() {
    let syst = unsafe_periph!(SYST);
    // // _VectorsRam[14] = pendablesrvreq_isr;
    // // _VectorsRam[15] = systick_isr;

    // SYST_RVR = (SYSTICK_EXT_FREQ / 1000) - 1;
    unsafe { syst.rvr.write((SYSTICK_EXT_FREQ / 1000) - 1) };

    // SYST_CVR = 0;
    unsafe { syst.cvr.write(0) };

    // SYST_CSR = SYST_CSR_TICKINT | SYST_CSR_ENABLE;
    // skip SYST_CSR_TICKINT=2 (enable interrupt)
    // set SYST_CSR_ENABLE=1 (enable countdown)
    // unset SYST_CSR_CLKSOURCE=4 (0 means external clock)
    unsafe { syst.csr.write(1) };

    // // ARM_DEMCR |= ARM_DEMCR_TRCENA;
    // dcb.enable_trace();

    // // ARM_DWT_CTRL |= ARM_DWT_CTRL_CYCCNTENA; // turn on cycle counter
    // dwt.enable_cycle_counter();

    // // systick_cycle_count = ARM_DWT_CYCCNT; // compiled 0, corrected w/1st systick
    // systick_cycle_count = dwt.get_cycle_count();
}

fn usb_pll_start() {
    let ccm_analog = unsafe_periph!(CCM_ANALOG);
    loop {
        // uint32_t n = CCM_ANALOG_PLL_USB1; // pg 759
        // printf("CCM_ANALOG_PLL_USB1=%08lX\n", n);
        let st = ccm_analog.pll_usb1.read();

        // if (n & CCM_ANALOG_PLL_USB1_DIV_SELECT) {
        if st.div_select().bit_is_set() {
            // printf("  ERROR, 528 MHz mode!\n"); // never supposed to use this mode!
            // CCM_ANALOG_PLL_USB1_CLR = 0xC000;                   // bypass 24 MHz
            ccm_analog
                .pll_usb1_clr
                .write_with_zero(|w| unsafe { w.bypass_clk_src().bits(0b11) });

            // CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_BYPASS;       // bypass
            ccm_analog
                .pll_usb1_set
                .write_with_zero(|w| w.bypass().set_bit());

            // CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_POWER |       // power down
            //                 CCM_ANALOG_PLL_USB1_DIV_SELECT |            // use 480 MHz
            //         CCM_ANALOG_PLL_USB1_ENABLE |                        // disable
            //                 CCM_ANALOG_PLL_USB1_EN_USB_CLKS;            // disable usb
            ccm_analog.pll_usb1_clr.write_with_zero(|w| {
                w.power()
                    .set_bit() // power down
                    .div_select()
                    .set_bit() // use 480MHz
                    .enable()
                    .set_bit() // disable
                    .en_usb_clks()
                    .set_bit() // disable usb
            });

            continue; // continue;
        } // }

        // if (!(n & CCM_ANALOG_PLL_USB1_ENABLE)) {
        if st.enable().bit_is_clear() {
            // printf("  enable PLL\n");
            // TODO: should this be done so early, or later??
            // CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_ENABLE;
            ccm_analog
                .pll_usb1_set
                .write_with_zero(|w| w.enable().set_bit());
            continue; // continue;
        } // }

        // if (!(n & CCM_ANALOG_PLL_USB1_POWER)) {
        if st.power().bit_is_clear() {
            // printf("  power up PLL\n");
            // CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_POWER;
            ccm_analog
                .pll_usb1_set
                .write_with_zero(|w| w.power().set_bit());
            continue; // continue;
        } // }

        // if (!(n & CCM_ANALOG_PLL_USB1_LOCK)) {
        if st.lock().bit_is_clear() {
            // printf("  wait for lock\n");
            continue; // continue;
        } // }

        // if (n & CCM_ANALOG_PLL_USB1_BYPASS) {
        if st.bypass().bit_is_set() {
            // printf("  turn off bypass\n");
            // CCM_ANALOG_PLL_USB1_CLR = CCM_ANALOG_PLL_USB1_BYPASS;
            ccm_analog
                .pll_usb1_clr
                .write_with_zero(|w| w.bypass().set_bit());
            continue; // continue;
        } // }

        // if (!(n & CCM_ANALOG_PLL_USB1_EN_USB_CLKS)) {
        if st.en_usb_clks().bit_is_clear() {
            // printf("  enable USB clocks\n");
            // CCM_ANALOG_PLL_USB1_SET = CCM_ANALOG_PLL_USB1_EN_USB_CLKS;
            ccm_analog
                .pll_usb1_set
                .write_with_zero(|w| w.en_usb_clks().set_bit());
            continue; // continue;
        } // }

        return; // everything is as it should be  :-)
    }
}

// A brief explanation of F_CPU_ACTUAL vs F_CPU
//  https://forum.pjrc.com/threads/57236?p=212642&viewfull=1#post212642
//volatile uint32_t F_CPU_ACTUAL = 396000000;
//volatile uint32_t F_BUS_ACTUAL = 132000000;

// Define these to increase the voltage when attempting overclocking
// The frequency step is how quickly to increase voltage per frequency
// The datasheet says 1300 is the absolute maximum voltage.  The hardware
// can actually create up to 1575, but going over 1300 risks damage!
//#define OVERCLOCK_STEPSIZE  28000000
//#define OVERCLOCK_MAX_VOLT  1300
//#define OVERCLOCK_MAX_VOLT  1575 // Danger Will Robinson!

fn set_arm_clock(frequency: u32) -> u32 {
    let ccm = unsafe_periph!(CCM);
    let ccm_analog = unsafe_periph!(CCM_ANALOG);
    let dcdc = unsafe_periph!(DCDC);

    let voltage_mv: u32 = if frequency > 528_000_000 {
        1250
    } else if false && frequency > 600_000_000 {
        // #if defined(OVERCLOCK_STEPSIZE) && defined(OVERCLOCK_MAX_VOLT)
        // voltage += ((frequency - 600000000) / OVERCLOCK_STEPSIZE) * 25;
        // if (voltage > OVERCLOCK_MAX_VOLT) voltage = OVERCLOCK_MAX_VOLT;
        0
    } else if frequency <= 24_000_000 {
        950
    } else {
        1150
    };

    // // if voltage needs to increase, do it before switch clock speed
    // CCM_CCGR6 |= CCM_CCGR6_DCDC(CCM_CCGR_ON);
    ccm.ccgr6.modify(|_, w| unsafe { w.cg3().bits(3) });

    let voltage_trg: u8 = ((voltage_mv - 800) / 25) as u8;
    // if ((dcdc & DCDC_REG3_TRG_MASK) < DCDC_REG3_TRG((voltage - 800) / 25)) {
    if dcdc.reg3.read().trg().bits() < voltage_trg {
        // FIXME: double read
        dcdc.reg3.modify(|_r, w| {
            // printf("Increasing voltage to %u mV\n", voltage);
            // dcdc &= ~DCDC_REG3_TRG_MASK;
            // dcdc |= DCDC_REG3_TRG((voltage - 800) / 25);
            // DCDC_REG3 = dcdc;
            unsafe { w.trg().bits(voltage_trg) }
        });
        // while (!(DCDC_REG0 & DCDC_REG0_STS_DC_OK)) ; // wait voltage settling
        while !dcdc.reg0.read().sts_dc_ok().bit() {
            asm::nop();
        }
    }

    // if (!(cbcdr & CCM_CBCDR_PERIPH_CLK_SEL)) {
    let cbcdr_r = ccm.cbcdr.read();
    if cbcdr_r.periph_clk_sel().is_periph_clk_sel_0() {
        // printf("need to switch to alternate clock during reconfigure of ARM PLL\n");
        // const uint32_t need1s = CCM_ANALOG_PLL_USB1_ENABLE | CCM_ANALOG_PLL_USB1_POWER |
        // CCM_ANALOG_PLL_USB1_LOCK | CCM_ANALOG_PLL_USB1_EN_USB_CLKS;
        // uint32_t sel, div;

        // if ((CCM_ANALOG_PLL_USB1 & need1s) == need1s) {
        let pll_r = ccm_analog.pll_usb1.read();
        let (sel, div) = if pll_r.enable().bit_is_set()
            && pll_r.power().bit_is_set()
            && pll_r.lock().bit_is_set()
            && pll_r.en_usb_clks().bit_is_set()
        {
            // printf("USB PLL is running, so we can use 120 MHz\n");
            // sel = 0;
            // div = 3; // divide down to 120 MHz, so IPG is ok even if IPG_PODF=0
            (
                dev::ccm::cbcdr::PERIPH_CLK_SEL_A::PERIPH_CLK_SEL_0,
                dev::ccm::cbcdr::PERIPH_CLK2_PODF_A::PERIPH_CLK2_PODF_3,
            )
        } else {
            // } else {
            // printf("USB PLL is off, use 24 MHz crystal\n");
            // sel = 1;
            // div = 0;
            (
                dev::ccm::cbcdr::PERIPH_CLK_SEL_A::PERIPH_CLK_SEL_0,
                dev::ccm::cbcdr::PERIPH_CLK2_PODF_A::PERIPH_CLK2_PODF_0,
            )
        }; // }

        // if ((cbcdr & CCM_CBCDR_PERIPH_CLK2_PODF_MASK) != CCM_CBCDR_PERIPH_CLK2_PODF(div)) {
        // PERIPH_CLK2 divider needs to be changed
        // cbcdr &= ~CCM_CBCDR_PERIPH_CLK2_PODF_MASK;
        // cbcdr |= CCM_CBCDR_PERIPH_CLK2_PODF(div);
        // CCM_CBCDR = cbcdr;
        // }
        ccm.cbcdr.modify(|_r, w| w.periph_clk2_podf().variant(div));

        // if ((cbcmr & CCM_CBCMR_PERIPH_CLK2_SEL_MASK) != CCM_CBCMR_PERIPH_CLK2_SEL(sel)) {
        //     // PERIPH_CLK2 source select needs to be changed
        //     cbcmr &= ~CCM_CBCMR_PERIPH_CLK2_SEL_MASK;
        //     cbcmr |= CCM_CBCMR_PERIPH_CLK2_SEL(sel);
        //     CCM_CBCMR = cbcmr;
        //     while (CCM_CDHIPR & CCM_CDHIPR_PERIPH2_CLK_SEL_BUSY) ; // wait
        // }
        ccm.cbcdr.modify(|_r, w| w.periph_clk_sel().variant(sel));
        while ccm
            .cdhipr
            .read()
            .periph2_clk_sel_busy()
            .is_periph2_clk_sel_busy_1()
        {
            asm::nop();
        }

        // // switch over to PERIPH_CLK2
        // cbcdr |= CCM_CBCDR_PERIPH_CLK_SEL;
        // CCM_CBCDR = cbcdr;
        ccm.cbcdr
            .modify(|_r, w| w.periph_clk_sel().periph_clk_sel_1());

        // while (CCM_CDHIPR & CCM_CDHIPR_PERIPH_CLK_SEL_BUSY) ; // wait
        while ccm
            .cdhipr
            .read()
            .periph_clk_sel_busy()
            .is_periph_clk_sel_busy_1()
        {
            asm::nop();
        }
    } // } else {
      //     printf("already running from PERIPH_CLK2, safe to mess with ARM PLL\n");
      // }

    // // TODO: check if PLL2 running, can 352, 396 or 528 can work? (no need for ARM PLL)

    // // DIV_SELECT: 54-108 = official range 648 to 1296 in 12 MHz steps
    // uint32_t div_arm = 1;
    // uint32_t div_ahb = 1;
    // while (frequency * div_arm * div_ahb < 648000000) {
    //     if (div_arm < 8) {
    //         div_arm = div_arm + 1;
    //     } else {
    //         if (div_ahb < 5) {
    //             div_ahb = div_ahb + 1;
    //             div_arm = 1;
    //         } else {
    //             break;
    //         }
    //     }
    // }

    let mut div_arm = 1u32;
    let mut div_ahb = 1u32;
    while frequency * div_arm * div_ahb < 648000000 {
        if div_arm < 8 {
            div_arm += 1;
        } else if div_ahb < 5 {
            div_ahb += 1;
            div_arm = 1;
        } else {
            break;
        }
    }

    // uint32_t mult = (frequency * div_arm * div_ahb + 6000000) / 12000000;
    // if (mult > 108) mult = 108;
    // if (mult < 54) mult = 54;
    let mult: u8 = max(
        54,
        min(
            108,
            ((frequency * div_arm * div_ahb + 6_000_000) / 12_000_000) as u8,
        ),
    );
    // FIXME: valid range for freq?

    // printf("Freq: 12 MHz * %u / %u / %u\n", mult, div_arm, div_ahb);
    let frequency = mult as u32 * 12000000 / div_arm / div_ahb;

    // printf("ARM PLL=%x\n", CCM_ANALOG_PLL_ARM);
    // const uint32_t arm_pll_mask = CCM_ANALOG_PLL_ARM_LOCK | CCM_ANALOG_PLL_ARM_BYPASS |
    // CCM_ANALOG_PLL_ARM_ENABLE | CCM_ANALOG_PLL_ARM_POWERDOWN |
    // CCM_ANALOG_PLL_ARM_DIV_SELECT_MASK;
    // if ((CCM_ANALOG_PLL_ARM & arm_pll_mask) != (CCM_ANALOG_PLL_ARM_LOCK
    //                                             | CCM_ANALOG_PLL_ARM_ENABLE | CCM_ANALOG_PLL_ARM_DIV_SELECT(mult))) {
    let pll_arm_r = ccm_analog.pll_arm.read();
    if pll_arm_r.lock().bit_is_clear()
        || pll_arm_r.enable().bit_is_clear()
        || pll_arm_r.bypass().bit_is_set()
        || pll_arm_r.div_select().bits() != mult
    {
        // printf("ARM PLL needs reconfigure\n");
        // CCM_ANALOG_PLL_ARM = CCM_ANALOG_PLL_ARM_POWERDOWN;
        ccm_analog
            .pll_arm
            .write_with_zero(|w| w.powerdown().set_bit());
        // TODO: delay needed?

        // CCM_ANALOG_PLL_ARM = CCM_ANALOG_PLL_ARM_ENABLE
        //     | CCM_ANALOG_PLL_ARM_DIV_SELECT(mult);
        ccm_analog
            .pll_arm
            .write_with_zero(|w| unsafe { w.enable().set_bit().div_select().bits(mult) });

        // while (!(CCM_ANALOG_PLL_ARM & CCM_ANALOG_PLL_ARM_LOCK)) ; // wait for lock
        while ccm_analog.pll_arm.read().lock().bit_is_clear() {
            asm::nop();
        }
        // printf("ARM PLL=%x\n", CCM_ANALOG_PLL_ARM);
    } // } else {
      //     printf("ARM PLL already running at required frequency\n");
      // }

    let arm_podf = div_arm as u8 - 1;
    // if ((CCM_CACRR & CCM_CACRR_ARM_PODF_MASK) != (div_arm - 1)) {
    if u8::from(ccm.cacrr.read().arm_podf().variant()) != arm_podf {
        // CCM_CACRR = CCM_CACRR_ARM_PODF(div_arm - 1);
        ccm.cacrr.write(|w| w.arm_podf().bits(arm_podf));
        // while (CCM_CDHIPR & CCM_CDHIPR_ARM_PODF_BUSY) ; // wait
        while ccm.cdhipr.read().arm_podf_busy().bit_is_set() {
            asm::nop();
        }
    } // }

    let ahb_podf = div_ahb as u8 - 1;
    // if ((cbcdr & CCM_CBCDR_AHB_PODF_MASK) != CCM_CBCDR_AHB_PODF(div_ahb - 1)) {
    if u8::from(ccm.cbcdr.read().ahb_podf().variant()) != ahb_podf {
        // cbcdr &= ~CCM_CBCDR_AHB_PODF_MASK;
        // cbcdr |= CCM_CBCDR_AHB_PODF(div_ahb - 1);
        // CCM_CBCDR = cbcdr;
        // FIXME: double read
        ccm.cbcdr.modify(|_r, w| w.ahb_podf().bits(ahb_podf));
        // while (CCM_CDHIPR & CCM_CDHIPR_AHB_PODF_BUSY); // wait
        while ccm.cdhipr.read().ahb_podf_busy().bit_is_set() {
            asm::nop();
        }
    } // }

    // uint32_t div_ipg = (frequency + 149999999) / 150000000;
    // if (div_ipg > 4) div_ipg = 4;
    let div_ipg = max((frequency + 149999999) / 150000000, 4);

    let ipg_podf = div_ipg as u8 - 1;
    // if ((cbcdr & CCM_CBCDR_IPG_PODF_MASK) != (CCM_CBCDR_IPG_PODF(div_ipg - 1))) {
    if u8::from(ccm.cbcdr.read().ipg_podf().variant()) != ipg_podf {
        // cbcdr &= ~CCM_CBCDR_IPG_PODF_MASK;
        // cbcdr |= CCM_CBCDR_IPG_PODF(div_ipg - 1);
        // // TODO: how to safely change IPG_PODF ??
        // CCM_CBCDR = cbcdr;
        ccm.cbcdr.modify(|_r, w| w.ipg_podf().bits(ipg_podf));
    } // }

    // cbcdr &= ~CCM_CBCDR_PERIPH_CLK_SEL;
    // CCM_CBCDR = cbcdr;
    ccm.cbcdr.modify(|_r, w| w.periph_clk_sel().clear_bit());
    // while (CCM_CDHIPR & CCM_CDHIPR_PERIPH_CLK_SEL_BUSY) ; // wait
    while ccm.cdhipr.read().periph_clk_sel_busy().bit_is_set() {
        asm::nop();
    }

    // TODO: readable freqs
    // F_CPU_ACTUAL = frequency;
    // F_BUS_ACTUAL = frequency / div_ipg;

    // printf("New Frequency: ARM=%u, IPG=%u\n", frequency, frequency / div_ipg);

    // // if voltage needs to decrease, do it after switch clock speed
    // if ((dcdc & DCDC_REG3_TRG_MASK) > DCDC_REG3_TRG((voltage - 800) / 25)) {
    if dcdc.reg3.read().trg().bits() > voltage_trg {
        // printf("Decreasing voltage to %u mV\n", voltage);
        // dcdc &= ~DCDC_REG3_TRG_MASK;
        // dcdc |= DCDC_REG3_TRG((voltage - 800) / 25);
        // DCDC_REG3 = dcdc;
        dcdc.reg3
            .modify(|_r, w| unsafe { w.trg().bits(voltage_trg) });

        // while (!(DCDC_REG0 & DCDC_REG0_STS_DC_OK)) ; // wait voltage settling
        while !dcdc.reg0.read().sts_dc_ok().bit() {
            asm::nop();
        }
    } // }

    return frequency;
}

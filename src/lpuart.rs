use crate::dev;

pub use crate::dev::lpuart1::RegisterBlock;
use crate::generic::*;
use crate::hal::serial::Write;
use nb::{block, Error::WouldBlock};

// This is just a sketch to get a one-way debug console going.
//
// TODO: read, duplex rw, single wire half duplex?
// TODO: different bitrates, parity, etc
// TODO: interrupts
// TODO: CTS/RTS pins
// TODO: input/output triggers?
// TODO: rxfifo/txfifo
// TODO: reset (global register; separate state?)
// TODO: express configured/unconfigured state
// TODO: reset / reconfigure a configured port?

pub trait State {}
pub struct Tx;
pub struct Rx;

impl State for Tx {}
impl State for Rx {}
impl State for () {}

pub struct LPUART<T, TState>
where
    T: Deref<Target = RegisterBlock>,
    TState: State,
{
    lpuart: T,
    _state: PhantomData<TState>,
}

impl<T, TState> _Function for LPUART<T, TState>
where
    T: Deref<Target = RegisterBlock>,
    TState: State,
{
}

impl<T> LPUART<T, ()>
where
    T: Deref<Target = RegisterBlock>,
{
    /// Configure for transmitting.
    ///
    /// Assumes that CCM & IOMUXC are already configured.
    pub fn into_tx(self) -> LPUART<T, Tx> {
        // Hardcoded configuration
        // ~115200 baud, rest at defaults (8bit, one stop bit)
        self.lpuart
            .baud
            .write(|w| unsafe { w.osr().bits(25).sbr().bits(8) });

        // TX enable
        self.lpuart.ctrl.write(|w| w.te().set_bit());

        LPUART {
            lpuart: self.lpuart,
            _state: PhantomData,
        }
    }
}

impl<T> Write<u8> for LPUART<T, Tx>
where
    T: Deref<Target = RegisterBlock>,
{
    type Error = !;

    fn write(&mut self, word: u8) -> nb::Result<(), Self::Error> {
        self.flush()?;
        self.lpuart.data.write(|w| unsafe { w.bits(word as u32) });
        Ok(())
    }

    fn flush(&mut self) -> nb::Result<(), Self::Error> {
        if self.lpuart.stat.read().tdre().is_tdre_0() {
            return Err(WouldBlock);
        }
        Ok(())
    }
}

impl<T> core::fmt::Write for LPUART<T, Tx>
where
    T: Deref<Target = RegisterBlock>,
{
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        for b in s.as_bytes() {
            block!(self.write(*b)).ok();
        }
        Ok(())
    }
}

// TODO: manage pins
macro_rules! lpuart_dev {
    ($lpuart: ident, $ccgr: ident, $ccgr_cg: ident) => {
        pub type $lpuart = LPUART<dev::$lpuart, ()>;
        ccm_enable!($lpuart, $ccgr, $ccgr_cg);
        ccm_enable!(dev::$lpuart, $ccgr, $ccgr_cg);

        impl<TState: State> LPUART<dev::$lpuart, TState> {
            pub unsafe fn fiat() -> Self {
                Self {
                    lpuart: core::mem::transmute::<_, dev::$lpuart>(()),
                    _state: PhantomData,
                }
            }
        }
    };
}

lpuart_dev!(LPUART1, ccgr5, cg12);
lpuart_dev!(LPUART2, ccgr0, cg14);
lpuart_dev!(LPUART3, ccgr0, cg6);
lpuart_dev!(LPUART4, ccgr4, cg12);
// lpuart_dev!(LPUART5, ccgr3, cg1);
lpuart_dev!(LPUART6, ccgr3, cg3);
lpuart_dev!(LPUART7, ccgr5, cg13);
lpuart_dev!(LPUART8, ccgr6, cg7);

// TODO: DmamuxSource (different for tx & rx)

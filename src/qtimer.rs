use crate::generic::*;

pub use crate::dev::{
    tmr1::{self as tmr, RegisterBlock},
    TMR1, TMR2, TMR3, TMR4,
};

// impl<T> _Function for T where T: Deref<Target = dev::tmr1::RegisterBlock> {}
impl _Function for TMR1 {}
impl _Function for TMR2 {}
impl _Function for TMR3 {}
impl _Function for TMR4 {}

ccm_enable!(TMR1, ccgr6, cg13);
ccm_enable!(TMR2, ccgr6, cg14);
ccm_enable!(TMR3, ccgr6, cg15);
ccm_enable!(TMR4, ccgr6, cg8);

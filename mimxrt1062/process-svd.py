#!/usr/bin/env python3.8

INPUT = "MIMXRT1062.orig.svd"
OUTPUT = "MIMXRT1062.nice.svd"

from collections import Counter
from pprint import pprint
import xml.etree.ElementTree as ET
import logging as l
import re
import sys

# accidentally quadratic or worse; find a way to hash a tag maybe?
def elements_equal(e1, e2):
    if e1.tag != e2.tag:
        return False
    if e1.text != e2.text:
        return False
    if e1.tail != e2.tail:
        return False
    if e1.attrib != e2.attrib:
        return False
    if len(e1) != len(e2):
        return False
    return all(elements_equal(c1, c2) for c1, c2 in zip(e1, e2))


def parse_int(s):
    if s.startswith("0x"):
        return int(s[2:], 16)
    if s.startswith("0"):
        return int(s[1:], 8)
    return int(s)


tree = ET.parse(sys.argv[1])
root = tree.getroot()

# Remove the SECURITY_CONFIG field: it has duplicate enumeratedValues,
# and refman lists these bits as "reserved", so I don't really know
# how to fix it.
security_config_parent = root.find(".//field/name[.='SECURITY_CONFIG']/../..")
security_config = security_config_parent.find("./field/name[.='SECURITY_CONFIG']/..")
security_config_parent.remove(security_config)

# Try to deduplicate IOMUXC
# TODO: IOMUXC_{GPR,SNVS,SNVS_GPR}
from collections import Counter
from pprint import pprint
from hashlib import sha256

pads = {}
reg_dup_fields = ("size", "access", "resetValue", "resetMask")
for periph in tree.findall("./peripherals/peripheral"):
    pn = periph.find("./name").text
    if pn.startswith("IOMUXC"):
        for reg in periph.findall("./registers/register"):
            rn = reg.find("./name").text
            if rn.startswith("SW_PAD_CTL_"):
                fs = reg.find("./fields")
                cfs = ET.canonicalize(ET.tostring(fs), strip_text=True)
                cs = sha256()
                cs.update(cfs.encode("utf-8"))
                hcs = cs.hexdigest()
                if hcs not in pads:
                    pads[hcs] = {"pn": pn, "rn": rn, "qrn": f"{pn}.{rn}"}
                    for fn in reg_dup_fields:
                        pads[hcs][fn] = reg.find(f"./{fn}").text
                else:
                    reg.remove(fs)
                    orig = pads[hcs]
                    if orig["pn"] == pn:
                        reg.set("derivedFrom", orig["rn"])
                    else:
                        reg.set("derivedFrom", orig["qrn"])
                    for fn in reg_dup_fields:
                        try:
                            f = reg.find(f"./{fn}")
                        except KeyError:
                            # we don't have field and original has – should we worry?
                            print(f"{pn}.{rn} doesn't have {fn}")
                            continue
                        if f.text == orig[fn]:
                            reg.remove(f)
                        else:
                            print(f"{pn}.{rn} customizes {fn}: {orig[fn]} -> {f.text}")

# pprint(pads)
# sys.exit(0)

# deduplicate DMA
dma = tree.find("./peripherals/peripheral[name='DMA0']/registers")

# Make DCHPRI a list; unfortunately, we lose resetValue DCHPRI3 is
# first, but we save ~7KLOC and replace 32 separate types with one. We
# can make up for that by implementing relevant methods in the
# teensy-4 crate.
dchpri = dma.find("./register[name='DCHPRI3']")
dchpri.find("./name").text = "DCHPRI%s"
for element in ET.fromstring(
    """<bucket>
  <dimIndex>3,2,1,0,7,6,5,4,11,10,9,8,15,14,13,12,19,18,17,16,23,22,21,20,27,26,25,24,31,30,29,28</dimIndex>
  <dimIncrement>1</dimIncrement>
  <dim>32</dim>
</bucket>"""
):
    dchpri.insert(2, element)

# Remove misleading reset value
dchpri.remove(dchpri.find("./resetValue"))
dchpri.remove(dchpri.find("./resetMask"))

for register in dma.findall("./register"):
    name = register.find("./name").text
    if name.startswith("DCHPRI") and name != "DCHPRI%s":
        dma.remove(register)

tcd = ET.fromstring(
    f"""
<cluster>
  <name>TCD%s</name>
  <description>Transfer Control Descriptor</description>
  <dim>32</dim>
  <dimIncrement>32</dimIncrement>
  <addressOffset>0x1000</addressOffset>
</cluster>
"""
)
for register in dma.findall("./register"):
    name = register.find("./name")
    if name.text.startswith("TCD"):
        dma.remove(register)
    if name.text.startswith("TCD0_"):
        name.text = name.text[5:]
        offset = register.find("addressOffset")
        offset.text = hex(parse_int(offset.text) - 0x1000)
        tcd.append(register)
dma.append(tcd)

# Deduplicate XBAR (skip XBARB3 which is derivedFrom XBARB2)
for (periph_name, register_count) in (("XBARA1", 66), ("XBARB2", 8)):
    registers = tree.find(f"./peripherals/peripheral[name='{periph_name}']/registers")
    sel = registers.find("./register[name='SEL0']")
    sel.insert(0, ET.fromstring(f"<dim>{register_count}</dim>\n"))
    sel.insert(1, ET.fromstring("<dimIncrement>2</dimIncrement>\n"))

    tag = sel.find("./name")
    tag.text = tag.text.replace("0", "[%s]")

    tag = sel.find("./description")
    tag.text = tag.text.replace("0", "%s")

    tag = sel.find("./fields/field[name='SEL0']/description")
    tag.text = tag.text.replace("OUT0", "OUT(2*%s)")

    tag = sel.find("./fields/field[name='SEL1']/description")
    tag.text = tag.text.replace("OUT1", "OUT(2*%s+1)")

    for register in registers.findall("./register"):
        if register.find("./name").text.startswith("SEL"):
            registers.remove(register)
    registers.insert(0, sel)

# Find duplicate enumeratedValues sets and replace them with
# derivedFrom first definition
evss = []
for periph in tree.findall("./peripherals/peripheral"):
    periph_name = periph.find("./name").text

    # TODO: handle clusters
    for register in periph.findall("./registers/register"):
        register_name = register.find("./name").text
        for field in register.findall("./fields/field"):
            field_name = field.find("./name").text
            fqfn = f"{periph_name}.{register_name}.{field_name}"

            evs = field.find("./enumeratedValues")
            if evs is None:
                continue

            orig_evs = None
            for orig_evs_candidate in evss:
                if elements_equal(evs, orig_evs_candidate):
                    orig_evs = orig_evs_candidate
                    break

            if orig_evs is None:
                if evs.find("./name") is None:
                    name = ET.Element("name")
                    name.text = field_name
                    evs.insert(0, name)
                evss.append(evs)
                continue

            l.debug(
                f"enumeratedValues of {fqfn} is duplicate of {orig_evs_fqfn}, setting derivedFrom"
            )
            evs.clear()
            evs.set("derivedFrom", orig_evs.find("./name").text)

# TODO: clones in dma0 (TCD, maybe DHCPRI)

# This particular SVD includes EVs with multiple undercores in names,
# which triggers Rust's style warnings (which are denied by
# defaults). Process them.
MULTIPLE_UNDERSCORE_RE = re.compile(r"__+")
for ev_name in tree.findall(".//enumeratedValue/name"):
    # Do we need to log it?
    ev_name.text = MULTIPLE_UNDERSCORE_RE.sub("_", ev_name.text)

tree.write(sys.argv[2])

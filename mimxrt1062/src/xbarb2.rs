#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Crossbar B Select Register %s"]
    pub sel: [SEL; 8],
}
#[doc = "Crossbar B Select Register %s\n\nThis register you can [`read`](crate::generic::Reg::read), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about avaliable fields see [sel](sel) module"]
pub type SEL = crate::Reg<u16, _SEL>;
#[allow(missing_docs)]
#[doc(hidden)]
pub struct _SEL;
#[doc = "`read()` method returns [sel::R](sel::R) reader structure"]
impl crate::Readable for SEL {}
#[doc = "`write(|w| ..)` method takes [sel::W](sel::W) writer structure"]
impl crate::Writable for SEL {}
#[doc = "Crossbar B Select Register %s"]
pub mod sel;

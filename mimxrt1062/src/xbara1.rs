#[doc = r"Register block"]
#[repr(C)]
pub struct RegisterBlock {
    #[doc = "0x00 - Crossbar A Select Register %s"]
    pub sel: [SEL; 66],
    #[doc = "0x84 - Crossbar A Control Register 0"]
    pub ctrl0: CTRL0,
    #[doc = "0x86 - Crossbar A Control Register 1"]
    pub ctrl1: CTRL1,
}
#[doc = "Crossbar A Select Register %s\n\nThis register you can [`read`](crate::generic::Reg::read), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about avaliable fields see [sel](sel) module"]
pub type SEL = crate::Reg<u16, _SEL>;
#[allow(missing_docs)]
#[doc(hidden)]
pub struct _SEL;
#[doc = "`read()` method returns [sel::R](sel::R) reader structure"]
impl crate::Readable for SEL {}
#[doc = "`write(|w| ..)` method takes [sel::W](sel::W) writer structure"]
impl crate::Writable for SEL {}
#[doc = "Crossbar A Select Register %s"]
pub mod sel;
#[doc = "Crossbar A Control Register 0\n\nThis register you can [`read`](crate::generic::Reg::read), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about avaliable fields see [ctrl0](ctrl0) module"]
pub type CTRL0 = crate::Reg<u16, _CTRL0>;
#[allow(missing_docs)]
#[doc(hidden)]
pub struct _CTRL0;
#[doc = "`read()` method returns [ctrl0::R](ctrl0::R) reader structure"]
impl crate::Readable for CTRL0 {}
#[doc = "`write(|w| ..)` method takes [ctrl0::W](ctrl0::W) writer structure"]
impl crate::Writable for CTRL0 {}
#[doc = "Crossbar A Control Register 0"]
pub mod ctrl0;
#[doc = "Crossbar A Control Register 1\n\nThis register you can [`read`](crate::generic::Reg::read), [`reset`](crate::generic::Reg::reset), [`write`](crate::generic::Reg::write), [`write_with_zero`](crate::generic::Reg::write_with_zero), [`modify`](crate::generic::Reg::modify). See [API](https://docs.rs/svd2rust/#read--modify--write-api).\n\nFor information about avaliable fields see [ctrl1](ctrl1) module"]
pub type CTRL1 = crate::Reg<u16, _CTRL1>;
#[allow(missing_docs)]
#[doc(hidden)]
pub struct _CTRL1;
#[doc = "`read()` method returns [ctrl1::R](ctrl1::R) reader structure"]
impl crate::Readable for CTRL1 {}
#[doc = "`write(|w| ..)` method takes [ctrl1::W](ctrl1::W) writer structure"]
impl crate::Writable for CTRL1 {}
#[doc = "Crossbar A Control Register 1"]
pub mod ctrl1;

#![no_std]
#![no_main]
#![feature(never_type)]

// TODO: conditional modify() - read, check condition, modify if
// changed? maybe_modify() for FnOnce(R, W) -> Option<W>?
// trait to add w.from_r(r) automatically?

extern crate nb;
extern crate panic_halt;
extern crate void;

extern crate teensy_4 as t4;

use core::sync::atomic::{compiler_fence, Ordering};

use core::fmt::Write;
use cortex_m_rt::{self, entry, exception};
use t4::platform::asm;
use t4::prelude::*;

#[inline(always)]
fn infinite_loop() -> ! {
    loop {
        // add some side effect to prevent this from turning into a UDF instruction
        // see rust-lang/rust#28728 for details
        compiler_fence(Ordering::SeqCst);
    }
}

#[exception]
fn HardFault(_ef: &cortex_m_rt::ExceptionFrame) -> ! {
    let mut pin = unsafe { t4::gpio::Pin11::<t4::gpio::Output>::fiat() };

    let scb_hfsr = 0xE000_ED2C as *const u32; // hard fault status register

    if unsafe { core::ptr::read_volatile(scb_hfsr) } & (1 << 30) != 0 {
        // blink once for forced
        pin.set_high().ok();
        asm::delay(10_000_000);
        pin.set_low().ok();
        asm::delay(10_000_000);
    }
    pin.set_high().ok();

    infinite_loop()
}

#[allow(dead_code)]
fn sleep_ms(ms: u32, syst: &mut t4::dev::SYST) {
    syst.clear_current();
    for _i in 0..ms {
        while !syst.has_wrapped() {
            asm::nop();
        }
    }
}

#[exception]
fn SysTick() {
    // We steal the pin to avoid trying to pass it from main()
    unsafe { t4::gpio::PinLED::fiat() }.toggle().ok();
}

#[entry]
fn main() -> ! {
    let freq = t4::rt::init_teensy();

    let mut cpp = t4::CorePeripherals::take().unwrap();
    let pp = t4::Peripherals::take().unwrap();

    // Set pin 13 (LED) & 11 (hard fault) into output mode
    unsafe { t4::gpio::Pin11::<()>::fiat() }.into_output();
    unsafe { t4::gpio::Pin13::<()>::fiat() }.into_output();

    let _pin1 = unsafe { t4::iomuxc::Pin1::<()>::fiat().into_lpuart_tx() };
    let mut u = unsafe { t4::lpuart::LPUART6::fiat().enable_clock(&pp.CCM) }.into_tx();
    write!(u, "Lorem ipsum dolor sit amet, freq={}\r\n", freq).ok();

    unsafe { t4::platform::interrupt::enable() };

    // reconfigure SYST to fire every second
    cpp.SYST.disable_counter();
    cpp.SYST.set_reload(t4::rt::SYSTICK_EXT_FREQ - 1);
    cpp.SYST.clear_current();
    cpp.SYST.enable_counter();
    cpp.SYST.enable_interrupt();

    infinite_loop();
}

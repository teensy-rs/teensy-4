use std::env;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;

fn main() {
    // Put the linker script somewhere the linker can find it
    let out = &PathBuf::from(env::var_os("OUT_DIR").unwrap());
    File::create(out.join("memory.x"))
        .unwrap()
        .write_all(include_bytes!("memory.x"))
        .unwrap();

    // Duplicate link.x from cortex-m-rt, because teensy4's /
    // mxrt1062's memory setup is more complex than cortex-m-rt
    // expects. On cortex-m-rt upgrades it will probably need to be
    // updated, unless the changes can be generalized and pushed
    // upstream.
    let mut f = File::create(out.join("link-t4.x")).unwrap();
    f.write_all(include_bytes!("link.x.in")).unwrap();
    if env::var_os("CARGO_FEATURE_RT").is_some() {
        // *IMPORTANT*: The weak aliases (i.e. `PROVIDED`) must come *after* `EXTERN(__INTERRUPTS)`.
        // Otherwise the linker will ignore user defined interrupts and always populate the table
        // with the weak aliases.
        writeln!(
            f,
            r#"
/* Provides weak aliases (cf. PROVIDED) for device specific interrupt handlers */
/* This will usually be provided by a device crate generated using svd2rust (see `device.x`) */
INCLUDE device.x"#
        ).unwrap();
    };

    // This is thumbv7em, hardcode
    let max_int_handlers = 240;

    // checking the size of the interrupts portion of the vector table is sub-architecture dependent
    writeln!(
        f,
        r#"
/* ASSERT(SIZEOF(.vector_table) <= 0x{:x}, "
There can't be more than {1} interrupt handlers. This may be a bug in
your device crate, or you may have registered more than {1} interrupt
handlers."); */
"#,
        max_int_handlers * 4 + 0x40,
        max_int_handlers
    ).unwrap();

    println!("cargo:rustc-link-search={}", out.display());

    // Only re-run the build script when memory.x is changed,
    // instead of when any part of the source code changes.
    println!("cargo:rerun-if-changed=memory.x");
    println!("cargo:rerun-if-changed=link.x.in");
}

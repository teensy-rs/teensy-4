/* # Developer notes

- Symbols that start with a double underscore (__) are considered "private"

- Symbols that start with a single underscore (_) are considered "semi-public"; they can be
  overridden in a user linker script, but should not be referred from user code (e.g. `extern "C" {
  static mut __sbss }`).

- `EXTERN` forces the linker to keep a symbol in the final binary. We use this to make sure a
  symbol if not dropped if it appears in or near the front of the linker arguments and "it's not
  needed" by any of the preceding objects (linker arguments)

- `PROVIDE` is used to provide default values that can be overridden by a user linker script

- On alignment: it's important for correctness that the VMA boundaries of both .bss and .data *and*
  the LMA of .data are all 4-byte aligned. These alignments are assumed by the RAM initialization
  routine. There's also a second benefit: 4-byte aligned boundaries means that you won't see
  "Address (..) is out of bounds" in the disassembly produced by `objdump`.
*/

/* Provides information about the memory layout of the device */
/* This will be provided by the user (see `memory.x`) or by a Board Support Crate */
INCLUDE memory.x

/* # Entry point = reset vector */
ENTRY(Reset);
EXTERN(__RESET_VECTOR); /* depends on the `Reset` symbol */

/* # Exception vectors */
/* This is effectively weak aliasing at the linker level */
/* The user can override any of these aliases by defining the corresponding symbol themselves (cf.
   the `exception!` macro) */
EXTERN(__EXCEPTIONS); /* depends on all the these PROVIDED symbols */

EXTERN(DefaultHandler);

PROVIDE(NonMaskableInt = DefaultHandler);
EXTERN(HardFaultTrampoline);
PROVIDE(MemoryManagement = DefaultHandler);
PROVIDE(BusFault = DefaultHandler);
PROVIDE(UsageFault = DefaultHandler);
PROVIDE(SecureFault = DefaultHandler);
PROVIDE(SVCall = DefaultHandler);
PROVIDE(DebugMonitor = DefaultHandler);
PROVIDE(PendSV = DefaultHandler);
PROVIDE(SysTick = DefaultHandler);

PROVIDE(DefaultHandler = DefaultHandler_);
PROVIDE(HardFault = HardFault_);

/* # Interrupt vectors */
EXTERN(__INTERRUPTS); /* `static` variable similar to `__EXCEPTIONS` */

/* # Pre-initialization function */
/* If the user overrides this using the `pre_init!` macro or by creating a `__pre_init` function,
   then the function this points to will be called before the RAM is initialized. */
PROVIDE(__pre_init = DefaultPreInit);

/* # bootdata.c from teensy4/cores.c */
EXTERN(__FlexSPI_NOR_Config);

/* # Sections */
SECTIONS
{
  /* hard-code locations of boot configuration */
  __ImageVectorTable = ORIGIN(FLASH) + 0x1000; /* 32 bytes */
  __BootData = __ImageVectorTable + 32;        /* 12 bytes */
  __VectorTable = ALIGN(__BootData + 12, 1024); /* overkill alignment? */

  .text.progmem ORIGIN(FLASH) : {
    KEEP(*(.flashconfig))
    FILL(0xFF)
    . = ORIGIN(FLASH) + 0x1000;

    /* IVT, hardcoded bc it's simpler to express here */
    . = __ImageVectorTable;
    LONG(0x402000d1);           /* header */
    LONG(__VectorTable);        /* docs are wrong, needs to be vec table, not start addr */
    LONG(0);                    /* reserved */
    LONG(0);                    /* device configuration data */
    LONG(__BootData);
    LONG(__ImageVectorTable);
    LONG(0);                    /* command sequence file */
    LONG(0);                    /* reserved */

    /* Boot data */
    . = __BootData;
    LONG(ORIGIN(FLASH));        /* absolute address of the image */
    LONG(_flashimagelen);       /* size of the program image */
    LONG(0);                    /* plugin flag */

    /* Vector table */
    . = __VectorTable;
    /* Initial Stack Pointer (SP) value */
    LONG(0x20010000); /* 64K DTCM for boot, ResetHandler configures stack after ITCM/DTCM setup */

    /* Reset vector */
    KEEP(*(.vector_table.reset_vector)); /* this is the `__RESET_VECTOR` symbol */
    __reset_vector = .;

    /* Exceptions */
    KEEP(*(.vector_table.exceptions)); /* this is the `__EXCEPTIONS` symbol */
    __eexceptions = .;

    /* Device specific interrupts */
    KEEP(*(.vector_table.interrupts)); /* this is the `__INTERRUPTS` symbol */
    __einterrupts = .;

    KEEP(*(.startup))
    *(.text.progmem*)
    *(.rodata.progmem*)
    *(.progmem*)

    . = ALIGN(4);
    KEEP(*(.init))
    __preinit_array_start = .;
    KEEP (*(.preinit_array))
    __preinit_array_end = .;
    __init_array_start = .;
    KEEP (*(.init_array))
    __init_array_end = .;

    . = ALIGN(16);
  } > FLASH

  .text.itcm : AT(LOADADDR(.text.progmem) + SIZEOF(.text.progmem)) {
          . = . + 32; /* MPU to trap NULL pointer deref */
          *(.fastrun)
          *(.text*)
          *(.HardFault*)              /* FIXME */
          . = ALIGN(16);
  } > ITCM

  .data : AT(LOADADDR(.text.itcm) + SIZEOF(.text.itcm)) {
          *(.rodata*)
          *(.data*)
          . = ALIGN(16);
  } > DTCM

  .bss ALIGN(4) : {
          *(.bss*)
          *(COMMON)
          . = ALIGN(32);
          . = . + 32; /* MPU to trap stack overflow */
  } > DTCM

  .bss.dma (NOLOAD) : {
          *(.dmabuffers)
          . = ALIGN(16);
  } > RAM

  /* ### .uninit */
  .uninit (NOLOAD) : ALIGN(4)
  {
    . = ALIGN(1024);            /* FIXME: this is to align reallocated __VTOR */
    KEEP(*(.uninit.vtor))
    KEEP(*(.uninit .uninit.*));
    . = ALIGN(4);
  } > DTCM

  __stext = ADDR(.text.itcm);
  __etext = ADDR(.text.itcm) + SIZEOF(.text.itcm);
  __sitext = LOADADDR(.text.itcm);

  __sdata = ADDR(.data);
  __edata = ADDR(.data) + SIZEOF(.data);
  __sidata = LOADADDR(.data);

  __sbss = ADDR(.bss);
  __ebss = ADDR(.bss) + SIZEOF(.bss);

  __sheap = ADDR(.bss.dma) + SIZEOF(.bss.dma);

  __ocram_start = ORIGIN(RAM);
  __ocram_size = LENGTH(RAM);
  __ocram_end = __ocram_start + __ocram_size;


  /* _flexram_bank_config & _stack_start are read in pre-init and used
   * to configure FlexRAM and then move SP */
  _itcm_block_count = (SIZEOF(.text.itcm) + 0x7FFE) >> 15;
  _flexram_bank_config = 0xAAAAAAAA | ((1 << (_itcm_block_count * 2)) - 1);
  _stack_start = ORIGIN(DTCM) + ((16 - _itcm_block_count) << 15);

  _flashimagelen = SIZEOF(.text.progmem) + SIZEOF(.text.itcm) + SIZEOF(.data);
  _teensy_model_identifier = 0x24;

  /* ## .got */
  /* Dynamic relocations are unsupported. This section is only used to detect relocatable code in
     the input files and raise an error if relocatable code is found */
  .got (NOLOAD) :
  {
    KEEP(*(.got .got.*));
  }

  /* ## Discarded sections */
  /DISCARD/ :
  {
    /* Unused exception related info that only wastes space */
    *(.ARM.exidx);
    *(.ARM.exidx.*);
    *(.ARM.extab.*);
  }
}

/* Do not exceed this mark in the error messages below                                    | */
/* # Alignment checks */
ASSERT(ORIGIN(FLASH) % 4 == 0, "
ERROR(teensy-4/rt): the start of the FLASH region must be 4-byte aligned");

ASSERT(ORIGIN(RAM) % 4 == 0, "
ERROR(teensy-4/rt): the start of the RAM region must be 4-byte aligned");

ASSERT(__sdata % 4 == 0 && __edata % 4 == 0, "
BUG(teensy-4/rt): .data is not 4-byte aligned");

ASSERT(__sidata % 4 == 0, "
BUG(teensy-4/rt): the LMA of .data is not 4-byte aligned");

ASSERT(__sbss % 4 == 0 && __ebss % 4 == 0, "
BUG(teensy-4/rt): .bss is not 4-byte aligned");

ASSERT(__sheap % 4 == 0, "
BUG(teensy-4/rt): start of .heap is not 4-byte aligned");

/* # Position checks */

/* ## .vector_table */
ASSERT(__reset_vector == __VectorTable + 0x8, "
BUG(teensy-4/rt): the reset vector is missing");

ASSERT(__eexceptions == __VectorTable + 0x40, "
BUG(teensy-4/rt): the exception vectors are missing");

ASSERT(__einterrupts - __VectorTable > 0x40, "
ERROR(teensy-4/rt): The interrupt vectors are missing.
Possible solutions, from most likely to less likely:
- Link to a svd2rust generated device crate
- Disable the 'device' feature of cortex-m-rt to build a generic application (a dependency
may be enabling it)
- Supply the interrupt handlers yourself. Check the documentation for details.");

/* # Other checks */
ASSERT(SIZEOF(.got) == 0, "
ERROR(teensy-4/rt): .got section detected in the input object files
Dynamic relocations are not supported. If you are linking to C code compiled using
the 'cc' crate then modify your build script to compile the C code _without_
the -fPIC flag. See the documentation of the `cc::Build.pic` method for details.");
/* Do not exceed this mark in the error messages above                                    | */

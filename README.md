Teensy 4 Rust
=============

**Work in progress.** Seriously, it's not even unstable, it doesn't
really exist yet.

There's an initiall peripheral crate at [mimxrt1062](./mimxrt1062/),
maybe something appears here before I update this README, but
I wouldn't recommend trying to use it until I update README to say
so.

You have been warned.

Works only with `--release` profile, otherwise stack initialization
blows up. I probably should just write early init (FlexRAM /
.text.itcm / stack, maybe also VTOR relocation) in assembler and jump
to Rust afterwards. This might also make changes in cortex-m-rt
unnecessary.
